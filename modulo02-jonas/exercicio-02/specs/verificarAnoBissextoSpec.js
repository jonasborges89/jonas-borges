describe( 'verificarAnoBissexto', function() {

  beforeEach( function() {
    chai.should()
  } )

  it( 'deve retornar ano bissexto false', function() {
     const ano = 2016;
     const resultado = naoBissexto(ano);
     resultado.should.equal(false)
  } )

  it( 'deve retornar ano bissexto true', function(){
    const ano = 2017;
    const resultado = naoBissexto(ano);
    resultado.should.equal(true)
  });
})
