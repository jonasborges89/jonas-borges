
//exercicio 01
function calcularCirculo(object) {

  if (object.tipoCalculo === "A") {
    var area = Math.PI * Math.pow(object.raio, 2);
    return area;
  }

  if (object.tipoCalculo === "C") {
    var circunferencia = 2 * Math.PI * object.raio;
    return circunferencia;
  }
}

//exercicio 02
function naoBissexto(ano) {
  if ((ano % 400 === 0) || (ano % 4 === 0) && (ano % 100 != 0)) {
    return false;
  }

  return true;
}

//exercicio  03
function concatenarSemUndefined(var1 = '', var2 = '') {
  return `${var1}${var2}`;
}

//exercicio  04
function concatenarSemNull(var1, var2) {
  var1 = var1 !== null ? var1 : " ";
  var2 = var2 !== null ? var2 : " ";
  return var1 + var2;
}

//exercicio 05
function concatenarEmPaz(var1, var2) {
 var1 = var1 || '';
 var2 = var2 || '';
 return var1 + var2;
}

//exercicio 06
function adicionar(var1) {
  return function (var2) {
    return var1 + var2;
  }
}

//exercicio 07
function fibonacci(n) {
  if(n === 1 || n === 2){
    return 1;
  }

  var a = 1, b = 0, resultado = 0, i, temp;

  for (i = 1; i <= n; i++) {
    temp = a + b;
    resultado = resultado + temp;
    a = b;
    b = temp;
  }
  return resultado;
}
