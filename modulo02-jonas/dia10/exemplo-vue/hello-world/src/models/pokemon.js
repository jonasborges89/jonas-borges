export default class Pokemon {
  constructor( jsonVindoDaApi ) {
    this.id = jsonVindoDaApi.id
    this.nome = jsonVindoDaApi.name
    this.thumbUrl = jsonVindoDaApi.sprites.front_default
    this._altura = jsonVindoDaApi.height
    this._peso = jsonVindoDaApi.weight
    this.tipos = jsonVindoDaApi.types.map( t => t.type.name )
    this.estatisticas = jsonVindoDaApi.stats.map( s => s.stat.name + ': ' + s.base_stat  )
  }

  get altura() {
    return this._altura * 10
  }

  get peso(){
    return this._peso / 10.0
  }
}