var tempo = 30;
const h1 = document.getElementById('horario');

function finalCountdown() {
  if (tempo >= 0) {
    h1.innerText = tempo >= 10 ? "00:00:" + tempo : "00:00:0" + tempo;
    tempo--;
    setTimeout('finalCountdown()', 1000);
  } else {
    h1.innerText = "Tempo encerrado!";
  }
}

finalCountdown();
