class Relogio {

  constructor() {
      this.atualizarH1 = function () {
      this.h1 = document.getElementById('horario');
      this.h1.innerText = moment().locale('pt-br').format('LTS');
    }
    this.atualizarH1();
    this.idIntervalo = setInterval(this.atualizarH1, 1000);
  }

  pararRelogio() {
    clearInterval(this.idIntervalo);
  }

  reiniciarRelogio() {
    this.idIntervalo = setInterval(this.atualizarH1, 1000)
  }
}

function rodarPrograma() {
  const btnPararRelogio = document.getElementById('btnPararRelogio');
  const btnReiniciarRelogio = document.getElementById('btnReiniciarRelogio');
  const relogio = new Relogio();
  btnPararRelogio.onclick = function () {
    relogio.pararRelogio();
  }

  btnReiniciarRelogio.onclick = function () {
    relogio.reiniciarRelogio();
  }
}

rodarPrograma();
