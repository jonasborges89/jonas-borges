
class PokeApi {
  constructor( id ) {
    this.idPokemon = id;
    this.url = 'https://pokeapi.co/api/v2/pokemon/'+ this.idPokemon + '/';
  }

  buscar() {
    return fetch( this.url )
  }
}