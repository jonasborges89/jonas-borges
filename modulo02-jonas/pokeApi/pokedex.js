function rodarPrograma() {
  
  const $txtIdPokemon = document.getElementById('idPokemon')
  const $btnEstouComSorte = document.getElementById('btnEstouComSorte')
  const $btnLimparIds = document.getElementById('btnLimparLocalStorage')
  const $dadosPokemon = document.getElementById('dadosPokemon')
  const $idPokemon = $dadosPokemon.querySelector('#idPok')
  const $h1 = $dadosPokemon.querySelector('h1')
  const $img = $dadosPokemon.querySelector('#thumb')
  const $altura = $dadosPokemon.querySelector('#altura')
  const $peso = $dadosPokemon.querySelector('#peso')
  const $tipos = $dadosPokemon.querySelector('#tipos')
  const $estatisticas = $dadosPokemon.querySelector('#estatisticas')
  localStorage.setItem('ids', JSON.stringify([]))
  limparListas()

  //eventos dos botões
  $txtIdPokemon.onblur = function () {
    const idDigitado = $txtIdPokemon.value
    if (idDigitado === '' || idDigitado < 0 || idDigitado > 802) return
    if (idDigitadoIgualIdAtual(idDigitado)) return
    limparListas()
    localStorage.id = idDigitado
    criarEBuscarPokemon(idDigitado)
  }

  $btnEstouComSorte.onclick = function () {
    limparListas()
    const idGerado = Math.floor(Math.random() * 802 + 1)
    var arrayLocalStorage = JSON.parse(localStorage.getItem('ids'))
    if (arrayLocalStorage[idGerado]) return
    arrayLocalStorage[idGerado] = idGerado
    localStorage.setItem('ids', JSON.stringify(arrayLocalStorage))
    if (idDigitadoIgualIdAtual(idGerado)) return
    criarEBuscarPokemon(idGerado)
  }

  $btnLimparIds.onclick = function () {
    localStorage.setItem('ids', JSON.stringify([]))
  }

  //funções da pokedesjs
  function renderizarPokemonNaTela(pokemon) {
    $h1.innerText = pokemon.nome.toUpperCase()
    $img.src = pokemon.thumbUrl
    $idPokemon.innerText = pokemon.id
    $altura.innerText = pokemon.altura + ' cm'
    $peso.innerText = pokemon.peso + ' kg'
    pokemon.tipos.forEach(tipo => {
      var li = document.createElement('li')
      li.appendChild(document.createTextNode(tipo))
      $tipos.appendChild(li)
    });
    pokemon.estatisticas.forEach(estatistica => {
      var li = document.createElement('li')
      li.appendChild(document.createTextNode(estatistica))
      $estatisticas.appendChild(li)
    })
  }

  function criarEBuscarPokemon(id) {
    const pokeApi = new PokeApi(id)
    pokeApi.buscar()
      .then(res => res.json())
      .then(dadosJson => {
        const pokemon = new Pokemon(dadosJson)
        renderizarPokemonNaTela(pokemon)
      })
  }

  function limparListas() {
    while ($tipos.hasChildNodes()) {
      $tipos.removeChild($tipos.firstChild)
    }
    while ($estatisticas.hasChildNodes()) {
      $estatisticas.removeChild($estatisticas.firstChild)
    }
  }

  function idDigitadoIgualIdAtual(id) {
    return id === localStorage.id
  }
}

rodarPrograma()
