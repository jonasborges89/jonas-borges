//exercicio 01
function somarPosicoesPares(array) {
  var resultado = 0;

  for (var i = 0; i < array.length; i++) {
    if (i % 2 === 0) {
      resultado += array[i];
    }
  }

  return resultado;
}

//exercicio 02
function formatarElfos(array) {

  for (var elfo of array) {
    var exp = elfo.experiencia > 0 ? " com experiencia" : " sem experiencia"
    var eFlechas = elfo.qtdFlechas > 1 ? " e com " + elfo.qtdFlechas + " flechas" : " e " + elfo.qtdFlechas + " flecha"
    elfo.nome = elfo.nome.toUpperCase()
    elfo.qtdFlechas = elfo.qtdFlechas  
    elfo.temExperiencia = elfo.experiencia > 0 ? true : false
    delete elfo.experiencia
    elfo.descricao = `${elfo.nome}${exp}${eFlechas}`
  }
  
  return array;
}
