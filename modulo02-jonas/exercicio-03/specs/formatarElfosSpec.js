describe('formatarArrayElfos', function () {

  beforeEach(function () {
    chai.should();
  });

  it('deveFormatarOsObjetosElfos', function () {
    var esperado = [{ nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 6, descricao: "LEGOLAS sem experiencia e com 6 flechas" },
    { nome: "GALANDRIEL", temExperiencia: true, qtdFlechas: 1, descricao: "GALANDRIEL com experiencia e 1 flecha" }]

    const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
    const elfo2 = { nome: "Galandriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [elfo1, elfo2]

    var resultado = formatarElfos(arrayElfos)
    resultado.should.eql(esperado)

  });
});
