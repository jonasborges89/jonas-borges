describe( 'formatarElfos', function() {
  beforeEach( function() {
    chai.should()
  } )

  it ('array vazio deve retornar vazio', function() {
    formatarElfos( [ ] ).should.eql( [ ] )
  } )

  describe( 'um objeto', function() {
    it ('apenas um objeto com experiência e com flechas', function() {
      const elfo1 = { nome: "Legolas", experiencia: 1, qtdFlechas: 6 }
      const esperado = [ { nome: "LEGOLAS", temExperiencia: true, qtdFlechas: 6, descricao: "LEGOLAS com experiência e com 6 flechas" } ]
      formatarElfos( [ elfo1 ] ).should.eql( esperado )
    } )
  
    it ('apenas um objeto sem experiência e sem flechas', function() {
      const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 0 }
      const esperado = [ { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 0, descricao: "LEGOLAS sem experiência e com 0 flechas" } ]
      formatarElfos( [ elfo1 ] ).should.eql( esperado )
    } )
  
    it ('apenas um objeto sem experiência e com flechas', function() {
      const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 2 }
      const esperado = [ { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 2, descricao: "LEGOLAS sem experiência e com 2 flechas" } ]
      formatarElfos( [ elfo1 ] ).should.eql( esperado )
    } )
  
    it ('apenas um objeto com experiência e sem flechas', function() {
      const elfo1 = { nome: "Legolas", experiencia: 1, qtdFlechas: 0 }
      const esperado = [ { nome: "LEGOLAS", temExperiencia: true, qtdFlechas: 0, descricao: "LEGOLAS com experiência e com 0 flechas" } ]
      formatarElfos( [ elfo1 ] ).should.eql( esperado )
    } )
  } )
} )
