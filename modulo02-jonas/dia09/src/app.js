

let app = new Vue({
  el: '#pokeApiHtml',
  data: {
    idParaBuscar: '',
    pokemon: {}
  },
  methods: {
    async buscar() {
      if(this.idDigitadoIgualIdAtual(this.idParaBuscar)) return
      localStorage.id = this.idParaBuscar
      const pokeapi = new PokeApi(this.idParaBuscar)
      this.pokemon = await pokeapi.buscar()
    },

    async buscaAleatoria() {
      this.criaArrayLocalStorage()
      const idGerado = Math.floor(Math.random() * 802 + 1)
      this.idParaBuscar = idGerado
      var arrayLocalStorage = JSON.parse(localStorage.getItem('ids')) 
      if (arrayLocalStorage[this.idParaBuscar]) return
      arrayLocalStorage[this.idParaBuscar] = this.idParaBuscar
      localStorage.setItem('ids', JSON.stringify(arrayLocalStorage))
      if (this.idDigitadoIgualIdAtual(this.idParaBuscar)) return
      const pokeapi = new PokeApi(this.idParaBuscar)
      this.pokemon = await pokeapi.buscar()
      
    },
    
    idDigitadoIgualIdAtual(id) {
      return id === localStorage.id
    },

    criaArrayLocalStorage(){
      if(localStorage.ids == null){
        localStorage.setItem('ids', JSON.stringify([]))
      }
    },

    limparLocalStorage(){
      localStorage.setItem('ids', JSON.stringify([]))
    }
  }
})
