const meuH2 = document.getElementById('tituloPagina')
const btn = document.getElementsByTagName('button')[0]

if (localStorage.count >= 5) {
  btn.disabled = true
}

if (!localStorage.count) {
  localStorage.count = 0
}

function perguntarNome() {
  const nome = prompt('Qual seu nome?')
  meuH2.innerText = nome
  localStorage.nome = nome
}

function renderizaNomeArmazenadoNaTela() {
  meuH2.innerText = localStorage.nome
}

const nomeArmazenado = localStorage.nome && localStorage.nome.length > 0
if (nomeArmazenado) {
  renderizaNomeArmazenadoNaTela()
} else {
  perguntarNome()
}

btn.onclick = function () {
  localStorage.count++;
  if (localStorage.count >= 5) {
    btn.disabled = true
    return;
  }
  localStorage.removeItem('nome');
  meuH2.innerText = ''
}

//PESQUISAR SOBRE WebDriverJS, Selenium, Protractor...
//PESQUISAR linguagem ubiqua
//meuH2.innerText = 'Carregando...'

//localStorage['nome'] = nome
//localStorage.setItem( 'nome', nome )
