describe( 'naoBissexto', function() {
  const expect = chai.expect
  beforeEach( function() {
    chai.should()
  } )

  it( '2016 é bissexto', function() {
    const ano = 2016
    var resultado = naoBissexto( ano )
    resultado.should.be.false
  } )

  it( '2012 é bissexto', function() {
    const ano = 2012
    var resultado = naoBissexto( ano )
    resultado.should.be.false
  } )

  it( '2017 não é bissexto', function() {
    const ano = 2017
    var resultado = naoBissexto( ano )
    resultado.should.be.true
  } )
} )
