class Jedi {
  constructor(nome) {
    this.nome = nome;
    this.h1 = document.createElement('h1');
    this.h1.innerText = this.nome;
    this.h1.id = `Jedi_${this.nome}`;
    this.dadosJedi = document.getElementById('dadosJedi');
    dadosJedi.appendChild(this.h1);
  }

  //atacando com replicação 
  atacarComEspelho() {
    const self = this
    setTimeout(function () {
      self.h1.innerText += ' atacou!';
    }, 1000)
  }

  //Amarrando o this com a função
  atacarBind() {
    setTimeout(function () {
      this.h1.innerText += ' atacou!';
    }.bind(this), 1000)
  }

  //Atacando com arrow function 
  atacar() {
    setTimeout(() => {
      this.h1.innerText += ' atacou!';
    }, 1000)
  }
}
