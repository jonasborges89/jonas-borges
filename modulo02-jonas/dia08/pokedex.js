function rodarPrograma() {
  const $dadosPokemon = document.getElementById('dadosPokemon')
  const $input = document.getElementById('idPokemon')
  const $h1 = $dadosPokemon.querySelector(' h1 ')
  const $img = $dadosPokemon.querySelector(' #thumb ')

  $input.onblur = function () {
    var id = $input.value
    let url = 'https://pokeapi.co/api/v2/pokemon/' + id + '/'
    buscarPokemon(url)
  }

  function buscarPokemon(url) {
    fetch(url).then(res => res.json()
    ).then(dadosJson => {
      $h1.innerText = dadosJson.name
      $img.src = dadosJson.sprites.front_default
    })
  }
}

rodarPrograma();
