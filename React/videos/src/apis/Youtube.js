import axios from 'axios';

const KEY = 'AIzaSyC2z0GdTEeHDE-4iovO4oXFsRCcSGM4JbA';

export default axios.create({
    baseURL : 'https://www.googleapis.com/youtube/v3',
    params : {
        part: 'snippet',
        maxResults: 5,
        key : KEY     
    }
});