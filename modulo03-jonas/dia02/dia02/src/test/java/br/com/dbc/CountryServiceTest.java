/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import br.com.dbc.entity.Country;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jonas.borges
 */
public class CountryServiceTest {
    
    public CountryServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class CountryService.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        CountryService result = CountryService.getInstance();
        CountryService expResult = result;
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCountryByName method, of class CountryService.
     */
    @Test
    public void testGetCountryByName() {
        System.out.println("getCountryByName");
        String nome = "%razi%";
        CountryService instance = CountryService.getInstance();
        String expResult = "Brazil";
        List<Country> result = instance.getCountryByName(nome);
        assertEquals(1, result.size());
        assertEquals(expResult, result.get(0).getNome());
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testaInsercao(){
        CountryService instance = CountryService.getInstance();
        Country mx = new Country();
        Country us = new Country();
        mx.setNome("Mexico");
        mx.setSigla("MX");
        us.setNome("Estado Unidos");
        us.setSigla("US");
        instance.persistCountry(us);
        instance.persistCountry(mx);
        String expected = "Mexico";
        List<Country> result = instance.getCountryByName(mx.getNome());
        assertEquals(expected, result.get(0).getNome());
    }
    
}
