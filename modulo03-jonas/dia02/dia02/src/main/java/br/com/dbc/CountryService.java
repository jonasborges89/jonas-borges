/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import br.com.dbc.entity.Country;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jonas.borges
 */
public class CountryService {

    private static CountryService instance;
    private final EntityManager con;

    public static CountryService getInstance() {
        return instance == null ? new CountryService() : instance;
    }

    private CountryService() {
        this.con = PersistenceUtils.getEm();
    }

    public List<Country> getCountryByName(String nome) {
        return PersistenceUtils.getEm().createQuery("select c from Country c where c.nome like :nome", Country.class)
                .setParameter("nome", nome).getResultList();
    }

    public void persistCountry(Country country) {

        try {

            this.con.getTransaction().begin();
            this.con.persist(country);
            this.con.getTransaction().commit();

        } catch (Exception ex) {

            ex.printStackTrace();
            this.con.getTransaction().rollback();

        }
    }

    public void updateCountry(Country country) {
        try {

            this.con.getTransaction().begin();
            this.con.merge(country);
            this.con.getTransaction().commit();

        } catch (Exception ex) {

            ex.printStackTrace();
            this.con.getTransaction().rollback();

        }
    }

    public void removeCountry(Country country) {
         try {

            this.con.getTransaction().begin();
            country = this.con.find(Country.class, country.getId());
            this.con.remove(country);
            this.con.getTransaction().commit();

        } catch (Exception ex) {

            ex.printStackTrace();
            this.con.getTransaction().rollback();

        }
    }

}
