/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import br.com.dbc.entity.Country;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jonas.borges
 */
public class dia2 {
    public static void main(String... args){
       EntityManagerFactory emf = Persistence.createEntityManagerFactory("dia02_pu");
       EntityManager em = emf.createEntityManager();
        System.out.println("teste");
        
        Country br = new Country();
        CountryService service = CountryService.getInstance();
        br.setNome("Canada");
        br.setSigla("CN");
        service.persistCountry(br);
    }
}
