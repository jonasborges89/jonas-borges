/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.SexoType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.createCriteria(Cliente.class).list().forEach(session::delete);
        session.createCriteria(Animal.class).list().forEach(session::delete);
        t.commit();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testaVariasInsercoes() {
        ClienteService instance = ClienteService.getInstance();
        Cliente jose = new Cliente();
        jose.setNome("José");
        jose.setSexo(SexoType.M);
        jose.setProfissao("Agricultor");
        Cliente carlos = new Cliente();
        carlos.setNome("Carlos");
        carlos.setProfissao("Carpinteiro");
        carlos.setSexo(SexoType.M);
        Cliente luisa = new Cliente();
        luisa.setNome("Luisa");
        luisa.setSexo(SexoType.F);
        luisa.setProfissao("Desenvolvedora");
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(jose);
        clientes.add(carlos);
        clientes.add(luisa);
        instance.createSeveralClientes(clientes);
        clientes = instance.findAll();
        int qtdEsperada = 3;
        assertEquals(qtdEsperada, clientes.size());
    }

    @Test
    public void criarDezClientesComDezAnimais() {
        ClienteService instanceClienteService = ClienteService.getInstance();
        AnimalService instanceAnimalService = AnimalService.getInstance();
        List<Cliente> clientes = new ArrayList<>();
        List<Animal> animaisParaInserirNoBanco = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Cliente c = new Cliente();
            c.setNome("Cliente " + i);
            c.setSexo(SexoType.F);
            c.setProfissao("Profissão " + i);
            clientes.add(c);
        }

        instanceClienteService.createSeveralClientes(clientes);
        List<Cliente> clientesVindosDoBanco = new ArrayList<>();
        clientesVindosDoBanco = instanceClienteService.findAll();
        assertEquals(10, clientesVindosDoBanco.size());
        
        for(int j = 0; j < clientesVindosDoBanco.size(); j ++){
            for(int i = 0; i < clientesVindosDoBanco.size(); i++){
                Animal an = new Animal();
                an.setNome("Animal " + i);
                an.setSexo(SexoType.M);
                an.setIdCliente(clientesVindosDoBanco.get(i));
                an.setValor(new BigDecimal(10.0));
                animaisParaInserirNoBanco.add(an);
            }
        }
        
        instanceAnimalService.createSeveralAnimais(animaisParaInserirNoBanco);
        List<Animal> animaisVindoDoBanco = instanceAnimalService.findAll();
        assertEquals(100.0, animaisVindoDoBanco.size(), 1);
        
        BigDecimal valorTotAnimaisCliente1 = instanceClienteService.valorTotAnimaisCliente(clientesVindosDoBanco.get(0).getId());
        BigDecimal valorEsperado = new BigDecimal(100.00);
        
        assertEquals(0, valorTotAnimaisCliente1.compareTo(valorEsperado));
        
    }

    @Test
    public void testeInsereComHibernate() {

        ClienteService instance = ClienteService.getInstance();
        List<Cliente> clientes = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Cliente c = new Cliente();
            c.setNome("Cliente " + i);
            c.setSexo(SexoType.F);
            c.setProfissao("Profissão " + i);
            clientes.add(c);
        }

        String nome = "Cliente 0";

        instance.createSeveralClientes(clientes);

        Session session = HibernateUtil.getSessionFactory().openSession();

        clientes = session
                .createCriteria(Cliente.class)
                .add(Restrictions.or(Restrictions.eq("id", new Long(1)),
                        Restrictions.eq("nome", "Cliente 0").ignoreCase()))
                .addOrder(Order.asc("id"))
                .list();

        assertEquals(nome, clientes.get(0).getNome());
    }

    @Test
    public void insereClienteViaHiberate() {
        Cliente c = new Cliente();
        c.setNome("jonas");
        c.setSexo(SexoType.M);
        c.setProfissao("Só deus sabe");
        Session session = HibernateUtil.getSessionFactory().openSession();

        Transaction t = session.beginTransaction();
        session.save(c);
        t.commit();

        List<Cliente> listCliente = new ArrayList<>();
        listCliente = session.createCriteria(Cliente.class).add(Restrictions.eq("nome", "jonas")).list();
        Long id = new Long(1);
        assertEquals(id, listCliente.get(0).getId(),1);
        assertEquals("jonas", listCliente.get(0).getNome());
        assertEquals("Só deus sabe", listCliente.get(0).getProfissao());
    }

}
