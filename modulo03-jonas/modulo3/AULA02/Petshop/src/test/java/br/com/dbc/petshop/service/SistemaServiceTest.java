/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.Sistema;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jonas
 */
public class SistemaServiceTest {

    public SistemaServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.createCriteria(Sistema.class).list().forEach(session::delete);
        session.createCriteria(Sistema.class).list().forEach(session::delete);
        t.commit();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class SistemaService.
     */
    @Test
    public void testFindAll() {

    }

    /**
     * Test of findAllCriteria method, of class SistemaService.
     */
    @Test
    public void testFindAllCriteria() {

    }

    /**
     * Test of create method, of class SistemaService.
     */
    @Test
    public void testCreate() {

    }

    /**
     * Test of createSeveralSistemas method, of class SistemaService.
     */
    @Test
    public void testCreateSeveralSistemas() {
       SistemaService instanceService = SistemaService.getInstance();
       List<Sistema> sistemasSecundarios = new ArrayList<>();
       Sistema w = new Sistema();
       w.setNome("Windows Vista");
       instanceService.createCriteria(w);
       Sistema mp = new Sistema();
       mp.setNome("Media Player");
       mp.setIdSistemaPai(w);
       Sistema nf = new Sistema();
       nf.setNome("Netflix");
       nf.setIdSistemaPai(w);
       sistemasSecundarios.add(mp);
       sistemasSecundarios.add(nf);
       instanceService.createSeveralSistemas(sistemasSecundarios);
       sistemasSecundarios.add(w);
       List<Sistema> sistemasVindosDoBanco = instanceService.findAll();
       assertEquals(sistemasSecundarios.size(),sistemasVindosDoBanco.size());
    }

}
