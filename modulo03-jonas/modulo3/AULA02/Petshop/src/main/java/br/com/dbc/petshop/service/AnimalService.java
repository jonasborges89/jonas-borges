/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
public class AnimalService {

    private static final AnimalService instance;

    static {
        instance = new AnimalService();
    }

    public static AnimalService getInstance() {
        return instance;
    }

    private AnimalService() {
    }
    
    public void AnimalPersist(Animal animal){
        
        EntityManager em = PersistenceUtils.getEm();
        
        try {
            
            em.getTransaction().begin();
            em.persist(animal);
            em.getTransaction().commit();
            
        } catch ( Exception e){
            
            e.printStackTrace();
            em.getTransaction().rollback();
            
        }
    }
    
    public List<Animal> findAll(){
        EntityManager em = PersistenceUtils.getEm();
        List<Animal> animais = em.createQuery("select a from Animal a").getResultList();
        return animais;
    }
    
    public List<Animal> findAllCriteria(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Animal.class).list();
    }
    
    public void createSeveralAnimais(List<Animal> animal){
        EntityManager em = PersistenceUtils.getEm();
        em.getTransaction().begin();
        animal.forEach((a) -> {
            em.persist(a);
        });
        em.getTransaction().commit();
    }
}
