/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.Sistema;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author jonas
 */
public class SistemaService {

    private static final SistemaService instance;

    static {
        instance = new SistemaService();
    }

    public static SistemaService getInstance() {
        return instance;
    }

    private SistemaService() {
    }

    public List<Sistema> findAll() {
        EntityManager em = PersistenceUtils.getEm();
        List<Sistema> sistemas = em.createQuery("select s from Sistema s").getResultList();
        return sistemas;
    }

    public List<Sistema> findAllCriteria() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Sistema.class).list();
    }

    public Sistema create(Sistema s) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            s = em.merge(s);
            em.getTransaction().commit();
            return s;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public Sistema createCriteria(Sistema s) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.save(s);
            t.commit();
            return s;
        } catch (Exception e) {
            t.rollback();
            throw e;
        }
    }

    public void createSeveralSistemas(List<Sistema> sistemas) {

        EntityManager em = PersistenceUtils.getEm();

        em.getTransaction().begin();

        sistemas.forEach((s) -> {
            em.persist(s);
        });

        em.getTransaction().commit();
    }

}
