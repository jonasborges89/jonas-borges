/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jonas
 */
@Entity
@Table(name = "SISTEMA")
@NamedQueries({
    @NamedQuery(name = "Sistema.findAll", query = "SELECT s FROM Sistema s")
    , @NamedQuery(name = "Sistema.findById", query = "SELECT s FROM Sistema s WHERE s.id = :id")
    , @NamedQuery(name = "Sistema.findByNome", query = "SELECT s FROM Sistema s WHERE s.nome = :nome")})
public class Sistema implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SISTEMA_SEQ")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "SISTEMA_SEQ", sequenceName = "SISTEMA_SEQ")
    private Long id;
    @Column(name = "NOME")
    private String nome;
    @OneToMany(mappedBy = "idSistemaPai")
    private List<Sistema> sistemaList;
    @JoinColumn(name = "ID_SISTEMA_PAI", referencedColumnName = "ID")
    @ManyToOne
    private Sistema idSistemaPai;

    public Sistema() {
    }

    public Sistema(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @XmlTransient
    public List<Sistema> getSistemaList() {
        return sistemaList;
    }

    public void setSistemaList(List<Sistema> sistemaList) {
        this.sistemaList = sistemaList;
    }

    public Sistema getIdSistemaPai() {
        return idSistemaPai;
    }

    public void setIdSistemaPai(Sistema idSistemaPai) {
        this.idSistemaPai = idSistemaPai;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sistema)) {
            return false;
        }
        Sistema other = (Sistema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dbc.petshop.entity.Sistema[ id=" + id + " ]";
    }

}
