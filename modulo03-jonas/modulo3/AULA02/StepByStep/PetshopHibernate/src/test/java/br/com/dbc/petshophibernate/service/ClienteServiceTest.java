/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.dao.HibernateUtil;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author jonas.borges
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreate() {
        System.out.println("create");

        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("animal 1")
                                .build()))
                .build();

        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();

        Assert.assertEquals("Quantidade de clientes errada", 1, clientes.size());
        Cliente result = clientes.stream().findAny().get();
        assertEquals("Cliente diferente", cliente1.getId(), result.getId());
        assertEquals("Quantidade animais diferente", cliente1.getAnimalList().size(), result.getAnimalList().size());
        assertEquals("Animais diferente", cliente1.getAnimalList().stream().findAny().get().getId(), result.getAnimalList().stream().findAny().get().getId());
        session.close();
    }

    @Test
    public void testeCreatedMocked() {
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(Arrays.asList(Animal.builder()
                        .nome("Animal 1")
                        .build()))
                .build();

        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente1);
        verify(daoMock, times(1)).createOrUpdate(cliente1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeCreateException() {

        System.out.println("create exception");
        ClienteService.getInstance().create(Cliente.builder().id(1l).build());

    }

    @Test
    public void testeUpdateMocked() {
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(Arrays.asList(Animal.builder()
                        .nome("Animal 1")
                        .build()))
                .build();

        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.update(cliente1);
        verify(daoMock, times(1)).createOrUpdate(cliente1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testeUpdateException() {

        System.out.println("create exception");
        ClienteService.getInstance().update(Cliente.builder().build());

    }

    @Test(expected = HibernateException.class)
    public void testeHibernateException() {
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(Arrays.asList(Animal.builder()
                        .nome("Animal 1")
                        .build()))
                .build();

        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(HibernateException.class).when(daoMock).createOrUpdate(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.update(cliente1);
        verify(daoMock, times(1)).createOrUpdate(cliente1);
    }
    
    @Test
    public void testeDeleteMocked() throws NotFoundException {
        System.out.println("create mocked");
        Cliente cliente1 = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(Arrays.asList(Animal.builder()
                        .nome("Animal 1")
                        .build()))
                .build();

        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Mockito.when(clienteService.findOne(cliente1.getId())).thenReturn(cliente1);
        clienteService.delete(cliente1.getId());
        verify(daoMock, times(1)).delete(cliente1);
    }
}
