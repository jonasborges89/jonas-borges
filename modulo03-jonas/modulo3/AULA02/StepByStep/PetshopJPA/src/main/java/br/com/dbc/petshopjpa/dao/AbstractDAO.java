/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.dao;

import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import static java.lang.String.format;

/**
 *
 * @author jonas.borges
 */
public abstract class AbstractDAO<E, I> {

    private static final Logger LOG = Logger.getLogger(ClienteDAO.class.getName());
    protected abstract Class<E> getEntityClass();

    public List<E> findAll() {
        EntityManager em = PersistenceUtils.getPersistence();
        return em.createQuery(
                format("select e from %s e", getEntityClass().getSimpleName()),getEntityClass()).getResultList();
    }

    public E findOne(I id) {
        EntityManager em = PersistenceUtils.getPersistence();
        em.getTransaction().begin();
        return em.createQuery("select c from Cliente c where c.id = :id", getEntityClass())
                .setParameter("id", id)
                .getSingleResult();
    }

    public void delete(I id) {
        EntityManager em = PersistenceUtils.getPersistence();
        try {
            em.getTransaction().begin();
            em.createQuery("delete from Cliente where id = :id")
                    .setParameter("id", id)
                    .executeUpdate();

            em.getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }

    }

    public void create(E entity) {
        EntityManager em = PersistenceUtils.getPersistence();
        try {
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }

    }

    public void update(Cliente cliente) {
        EntityManager em = PersistenceUtils.getPersistence();
        try {
            em.getTransaction().begin();
            em.merge(cliente);
            em.getTransaction().commit();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }

    }
}
