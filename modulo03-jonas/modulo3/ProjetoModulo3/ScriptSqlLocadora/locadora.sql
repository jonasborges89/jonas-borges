create table cliente (
id number primary key not null,
nome varchar2(100) not null,
telefone long,
endereco varchar(200)
);
create sequence cliente_seq;

create table filme (
id number primary key not null,
titulo varchar2(50),
categoria varchar2(50) check(categoria in ('ACAO','AVENTURA','ANIMACAO')),
lancamento date
);
create sequence filme_seq;

create table midia (
id number primary key not null, 
midia_type varchar2(50) check(midia_type in ('VHS','DVD','BLUE-RAY')), 
quantidade integer, 
id_filme number not null references filme(id)
);
create sequence midia_seq;

create table valor_midia(
id number primary key not null, 
valor float, 
inicio_vigencia date, 
fim_vigencia date,
id_midia number not null references midia(id)
);
create sequence valor_midia_seq;

create table aluguel(
id number primary key not null ,
id_cliente number not null references cliente(id),
data_retirada date, 
date_previsao date,
data_entrega date,
multa float
);
create sequence aluguel_seq;

create table aluguel_midia(
id number primary key not null,
id_aluguel number references aluguel(id),
id_midia number references midia(id)
);

create sequence aluguel_midia_seq;

