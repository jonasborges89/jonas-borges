/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name="MIDIA_SEQ", sequenceName = "MIDIA_SEQ", allocationSize = 1)
public class Midia implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "MIDIA_SEQ" )
    private Long id;
    
    private MidiaType tipo;
    
    @JoinColumn(name = "ID_ALUGUEL", referencedColumnName = "ID")
    @ManyToOne( optional = true)
    private Aluguel aluguel; 
    
    @NotNull
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne( optional = false )
    private Filme filme;
}
