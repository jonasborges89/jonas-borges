/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MidiaDTO {
    
    private MidiaType tipo;
    private double valor;
    private int quantidade;
    
    public Midia toMidia(Filme filme){
        return Midia.builder().tipo(this.tipo).filme(filme).build();
    }
}
