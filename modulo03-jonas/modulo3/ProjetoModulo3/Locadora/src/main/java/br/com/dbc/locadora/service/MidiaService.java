/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.dto.MidiaSearchDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jonas.borges
 */
@Service
public class MidiaService extends AbstractCrudService<Midia> {

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }

    public List<Midia> findByFilmeId(Filme filme) {
        return midiaRepository.findByFilmeId(filme.getId());
    }

    public Long countByTipo(MidiaType tipo) {
        return midiaRepository.countByTipo(tipo);
    }

    public List<Midia> findByAluguel(Long id) {
        return midiaRepository.findByAluguelId(id);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void salvaMidiaDTO(MidiaDTO midiaDTO, Filme filme) {
        for (int i = 0; i < midiaDTO.getQuantidade(); i++) {
            Midia midia = midiaRepository.save(midiaDTO.toMidia(filme));
            valorMidiaService.save(ValorMidia.builder()
                    .valor(midiaDTO.getValor())
                    .inicioVigencia(LocalDateTime.now())
                    .midia(midia).build());
        }
    }

    public Long countByIdFilmeAndTipo(Filme filme, MidiaType tipo) {
        return midiaRepository.countByFilmeIdAndTipo(filme.getId(), tipo);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateMidiaPorDTO(Filme filme, MidiaDTO midiaDTO) {

        List<Midia> midiasAtuais = midiaRepository.findByFilmeId(filme.getId())
                .stream().filter(midia -> midia.getTipo()
                .equals(midiaDTO.getTipo())).collect(Collectors.toList());

        //Se a quantidade for igual
        if (midiasAtuais.size() == midiaDTO.getQuantidade()) {
            for (int i = 0; i < midiasAtuais.size(); i++) {
                valorMidiaService.atualizaValoresMidia(midiasAtuais.get(i), midiaDTO);
            }
        }

        //se a nova quantidade for maior que a anterior
        if (midiasAtuais.size() < midiaDTO.getQuantidade()) {
            for (int i = 0; i < midiasAtuais.size(); i++) {
                valorMidiaService.atualizaValoresMidia(midiasAtuais.get(i), midiaDTO);
                midiaDTO.setQuantidade(midiaDTO.getQuantidade() - 1);
            }

            salvaMidiaDTO(midiaDTO, filme);
        }

        //se a nova quantidade for menor que a anterior
        if (midiasAtuais.size() > midiaDTO.getQuantidade()) {

            int diferenca = midiasAtuais.size() - midiaDTO.getQuantidade();

            List<Midia> midiasNaoLocadas = midiasAtuais.stream()
                    .filter(midia -> midia.getAluguel() == null)
                    .collect(Collectors.toList());

            for (int i = 0; i < diferenca; i++) {
                valorMidiaService.deleteByIdMidia(midiasNaoLocadas.get(i));
                delete(midiasNaoLocadas.get(i).getId());
            }

            List<Midia> midiasRestantes = midiaRepository.findByFilmeId(filme.getId())
                    .stream().filter(midia -> midia.getTipo().equals(midiaDTO.getTipo())).collect(Collectors.toList());;

            for (int i = 0; i < midiasRestantes.size(); i++) {
                valorMidiaService.atualizaValoresMidia(midiasRestantes.get(i), midiaDTO);
            }
        }
    }

    public Page<MidiaSearchDTO> searchMidia(Pageable pageable, String titulo, Categoria categoria,
            LocalDate lancamentoIni, LocalDate lancamentoFim) {

        lancamentoIni = lancamentoIni == null ? LocalDate.MIN : lancamentoIni;
        lancamentoFim = lancamentoFim == null ? LocalDate.MIN : lancamentoFim;
        List<MidiaSearchDTO> retornoDTO = new ArrayList<>();
        
        List<Filme> filmes = midiaRepository.findByFilmeTituloContainingIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween(pageable,
                titulo, categoria, lancamentoIni, lancamentoFim).getContent()
                .stream()
                .map(Midia::getFilme)
                .distinct()
                .collect(Collectors.toList());
        
        filmes.forEach((filme) -> {
            List<Midia> midiasDoFilme = midiaRepository.findByFilmeId(filme.getId());
            midiasDoFilme.forEach((midia) -> {
                ValorMidia valor = valorMidiaService.findByMidiaIdAndFimVigenciaIsNull(midia.getId());
                retornoDTO.add(MidiaSearchDTO.builder().build().toMidiaSearchDTO(filme, midia, valor));
            });
        });
        
        return new PageImpl<>(retornoDTO, pageable, retornoDTO.size());
    }

    public Midia findFirstByFilmeIdAndTipo(Long idFilme, MidiaType tipo) {
        return midiaRepository.findFirstByFilmeIdAndTipo(idFilme, tipo);
    }
    
    public Midia findFirstByFilmeIdAndTipoOrderByAluguelDataPrevisaoAsc(Long id, MidiaType tipo){
        return midiaRepository.findFirstByFilmeIdAndTipoOrderByAluguelDataPrevisaoAsc(id, tipo);
    }
}
