/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoDTO;
import br.com.dbc.locadora.dto.RetiradaDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jonas.borges
 */
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class AluguelService extends AbstractCrudService<Aluguel> {

    @Autowired
    private final AluguelRepository aluguelRepository;

    @Autowired
    private final ClienteService clienteService;

    @Autowired
    private final ValorMidiaService valorMidiaService;
    
//    @Autowired
//    private final FilmeService filmeService;

    @Autowired
    private final MidiaService midiaService;

    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public RetiradaDTO aluguelRetirada(AluguelDTO aluguelDTO) {
        
        List<ValorMidia> valoresMidias = new ArrayList<>();
        Optional<Cliente> clienteOptional = clienteService.findById(aluguelDTO.getIdCliente());
        Cliente cliente = clienteOptional.get();
        Aluguel aluguel = new Aluguel();
        aluguel.setIdCliente(cliente);
        aluguel.setDataRetirada(LocalDateTime.now());
        
        List<Midia> midias = new ArrayList<>();

        for (int i = 0; i < aluguelDTO.getMidias().size(); i++) {

            Optional<Midia> midiaOptional = midiaService.findById(aluguelDTO.getMidias().get(i));
            Midia midia = midiaOptional.get();
            if (midia.getAluguel() == null) {
                midias.add(midia);
                valoresMidias.add(valorMidiaService.findByMidiaIdAndInicioVigenciaBetween(aluguelDTO.getMidias().get(i), aluguel.getDataRetirada()));
            } 
        }
        
        Double valorAluguel = valorMidiaService.calculaValorAluguel(valoresMidias);
      
        int qtdDias = midias.size();
        aluguel.setDataPrevisao(LocalDate.now().plusDays(qtdDias).atTime(16, 0));
        aluguel = aluguelRepository.save(aluguel);

        for (Midia midia : midias) {
            midia.setAluguel(aluguel);
            midia = midiaService.save(midia);
        }
        
        RetiradaDTO retirada = RetiradaDTO.builder().build().toRetiradaDTO(aluguel, valorAluguel);
        return retirada;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public DevolucaoDTO aluguelDevolucao(AluguelDTO aluguelDTO) {

        List<Midia> midias = new ArrayList<>();
        List<ValorMidia> valoresMidias = new ArrayList<>();
        Double valorAluguel = 0.0;

        for (Long id : aluguelDTO.getMidias()) {
            Optional<Midia> midiaOptional = midiaService.findById(id);
            midias.add(midiaOptional.get());
        }

        Aluguel aluguel = aluguelRepository.findById(midias.get(0)
                .getAluguel().getId()).get();

        aluguel.setMulta(0.0);
        aluguel.setDataEntrega(LocalDateTime.now());

        for (Midia midia : midias) {
            valoresMidias.add(valorMidiaService.findByMidiaIdAndInicioVigenciaBetween(midia.getId(), aluguel.getDataRetirada()));
            midia.setAluguel(null);
            midiaService.save(midia);
        }

        valorAluguel = valorMidiaService.calculaValorAluguel(valoresMidias);

        if (aluguel.getDataEntrega().isAfter(aluguel.getDataPrevisao())) {
            aluguel.setMulta(valorAluguel);
        }
       
        aluguel = aluguelRepository.save(aluguel);

        return DevolucaoDTO.builder().build().toDevolucaoDTO(aluguel, valorAluguel);
    }
    
    public Page<Filme> devolucoesHoje(Pageable pageable){
        
        LocalDateTime inicioDoDia = LocalDate.now().atStartOfDay();
       
        LocalDateTime diaHoraFimEntregas = LocalDate.now().atTime(16, 0);
        List<Aluguel> alugueisParaHoje = aluguelRepository.findByDataPrevisaoBetween(pageable, inicioDoDia, diaHoraFimEntregas).getContent();
        
        List<Midia> midias = new ArrayList<>();
        List<Filme> filmesParaDevolucao = new ArrayList<>();
        
        alugueisParaHoje.forEach((aluguel) -> {
            midias.addAll(midiaService.findByAluguel(aluguel.getId()));
        });
        
        midias.forEach((midia) ->{
            filmesParaDevolucao.add(midiaService.findById(midia.getId()).get().getFilme());
        });
        
        return new PageImpl<>(filmesParaDevolucao, pageable, filmesParaDevolucao.size());
    }
    

}
