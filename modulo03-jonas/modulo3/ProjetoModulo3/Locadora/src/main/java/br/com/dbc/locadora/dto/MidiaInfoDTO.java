/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MidiaInfoDTO {
    
    public MidiaType tipo;
    
    public Double valor;
    
    public int quantidade;
    
    private Boolean disponivel;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING , pattern = "dd/MM/yyyy")
    private LocalDateTime previsaoDisponibilidade;
    
    @SuppressWarnings("empty-statement")
    public MidiaInfoDTO toMidiaInfoDTO(List<Midia> midias, Boolean disponivel, LocalDateTime previsao, Double valor){
        
        this.setValor(valor);
        this.setDisponivel(disponivel);
        this.setTipo(midias.get(0).getTipo());
        this.setQuantidade(midias.size());
        this.setPrevisaoDisponibilidade(previsao);
        return this;
    }
    
}
