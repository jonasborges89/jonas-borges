/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author jonas.borges
 */
@RestController
@RequestMapping("/api/aluguel")
public class AluguelRestController extends AbstractRestController<Aluguel, AluguelService> {

    @Autowired
    private AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }
    
    @PostMapping("/retirada")
    public ResponseEntity<?> aluguelRetirada(@RequestBody AluguelDTO aluguelDTO){
        return ResponseEntity.ok(getService().aluguelRetirada(aluguelDTO));
    }
    
    @PostMapping("/devolucao")
    public ResponseEntity<?> aluguelDevolucao(@RequestBody AluguelDTO aluguelDTO){
        return ResponseEntity.ok(getService().aluguelDevolucao(aluguelDTO));
    }
    
    @GetMapping("/devolucao")
    public ResponseEntity<?> devolucoesHoje(Pageable pageable){
        return ResponseEntity.ok(getService().devolucoesHoje(pageable));
    }

}
