/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AluguelDTO {

    private Long id;

    private Long idCliente;

    private List<Long> midias;
}
