/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MidiaSearchDTO {
    
    private Long id;
    
    private MidiaType tipo;
    
    private Double valor;
    
    private Filme filme;
    
    public MidiaSearchDTO toMidiaSearchDTO(Filme filme, Midia midia, ValorMidia valorMidia){
        this.setId(midia.getId());
        this.setTipo(midia.getTipo());
        this.setValor(valorMidia.getValor());
        this.setFilme(filme);
        
        return this;
    }
}
