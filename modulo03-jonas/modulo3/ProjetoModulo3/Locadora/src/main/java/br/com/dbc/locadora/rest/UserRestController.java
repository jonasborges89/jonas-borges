/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas
 */
@RestController
@RequestMapping("/api/user")
public class UserRestController {
    
    @Autowired
    private UserServiceImpl userServiceImpl;

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> novoUsuario(@RequestBody User user){
        return ResponseEntity.ok(userServiceImpl.novoUsuario(user));
    }
    
    @PostMapping("/password")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> updatePassword(@RequestBody User user){
        return ResponseEntity.ok(userServiceImpl.updatePassword(user));
    }
}
