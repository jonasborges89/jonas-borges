/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.ValorMidia;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jonas.borges
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {
 
    public List<ValorMidia> findByMidiaId(Long id);
    
    public void deleteByMidiaId(Long midia);
    
    public ValorMidia findByMidiaIdAndFimVigenciaIsNull(Long idMidia);
    
    @Query("select vl from ValorMidia vl "
            + "where vl.midia.id = :idMidia and vl.inicioVigencia <= :retirada "
            + "and (vl.fimVigencia >= :retirada or vl.fimVigencia is null)")
    ValorMidia findByMidiaIdAndInicioVigenciaBetween(@Param("idMidia") Long idMidia, @Param("retirada") LocalDateTime retirada);
}
