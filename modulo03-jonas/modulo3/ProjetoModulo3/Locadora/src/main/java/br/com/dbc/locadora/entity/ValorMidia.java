/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author jonas.borges
 */
@Entity
@Data
@Builder 
@NoArgsConstructor
@AllArgsConstructor
 @SequenceGenerator(name="VALOR_MIDIA_SEQ", sequenceName = "VALOR_MIDIA_SEQ", allocationSize = 1)
public class ValorMidia implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue ( strategy = GenerationType.SEQUENCE, generator = "VALOR_MIDIA_SEQ" )
    private Long id;
    
    private Double valor;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime inicioVigencia;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime fimVigencia;
    
    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Midia midia;
}
