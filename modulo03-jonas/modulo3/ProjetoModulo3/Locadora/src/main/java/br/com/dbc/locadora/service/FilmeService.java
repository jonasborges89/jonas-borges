/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.FilmeValoresDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.dto.MidiaInfoDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author jonas.borges
 */
@Service
public class FilmeService extends AbstractCrudService<Filme> {

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }

    public Page<Filme> search(Pageable pageable, String titulo, Categoria categoria,
            LocalDate lancamentoIni, LocalDate lancamentoFim) {

        lancamentoIni = lancamentoIni == null ? LocalDate.MIN : lancamentoIni;
        lancamentoFim = lancamentoFim == null ? LocalDate.MIN : lancamentoFim;

        return filmeRepository
                .findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(pageable, titulo,
                        categoria, lancamentoIni, lancamentoFim);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme SalvarFilmeComMidia(@RequestBody FilmeDTO dto) {

        Filme filme = getRepository().save(dto.toFilme());

        dto.getMidia().forEach((midiaDTO) -> {
            midiaService.salvaMidiaDTO(midiaDTO, filme);
        });

        return filme;
    }

    public Long countByIdFilmeAndTipo(Long id, MidiaType tipo) {
        Filme filme = findById(id).get();
        return midiaService.countByIdFilmeAndTipo(filme, tipo);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme updateFilmeEMidia(Long id, FilmeDTO filmeDTO) {
        Filme filme = save(filmeDTO.toFilme());

        for (MidiaDTO midiaDTO : filmeDTO.getMidia()) {
            midiaService.updateMidiaPorDTO(filme, midiaDTO);
        }

        return filme;
    }

    public Page<FilmeValoresDTO> findValoresByIdFilme(Pageable pageable, Long id) {
        Optional<Filme> filmeRetorno = filmeRepository.findById(id);
        List<FilmeValoresDTO> filmeValores = new ArrayList<>();
        Filme filme = filmeRetorno.get();

        List<Midia> midias = midiaService.findByFilmeId(filme);
        
        for(Midia midia : midias){
            List<ValorMidia> valores = valorMidiaService.findByMidiaId(midia.getId());
            
            for(ValorMidia valor : valores){
                filmeValores.add(FilmeValoresDTO.builder().build().toFilmeValoresDTO(midia, valor));
            }
            
        }
        
        return new PageImpl<>(filmeValores, pageable, filmeValores.size());
    }

    public Page<CatalogoDTO> buscarCatalogo(Pageable pageable, FilmeDTO filmeDTO) {

        List<Filme> filmes = filmeRepository.findByTituloContainingIgnoreCase(pageable, filmeDTO.getTitulo()).getContent();
        List<CatalogoDTO> catalogo = new ArrayList<>();

        for (Filme filme : filmes) {

            List<MidiaInfoDTO> infos = new ArrayList<>();

            List<Midia> midias = midiaService.findByFilmeId(filme);
            List<Midia> midiasVHS = new ArrayList<>();
            List<Midia> midiasDVD = new ArrayList<>();
            List<Midia> midiasBLUE = new ArrayList<>();

            for (Midia midia : midias) {
                if (midia.getTipo() == MidiaType.VHS) {
                    midiasVHS.add(midia);
                }
                if (midia.getTipo() == MidiaType.DVD) {
                    midiasDVD.add(midia);
                }
                if (midia.getTipo() == MidiaType.BLUE_RAY) {
                    midiasBLUE.add(midia);
                }
            }

            if (!midiasVHS.isEmpty()) {
                ValorMidia valor = valorMidiaService.findByMidiaIdAndFimVigenciaIsNull(midiasVHS.get(0).getId());

                Boolean disponivel = true;

                LocalDateTime previsao = null;

                List<Midia> midiasNaoAlugadas = midiasVHS
                        .stream().filter(midia -> midia.getAluguel() == null).collect(Collectors.toList());

                List<Midia> midiasAlugadas = midiasVHS
                        .stream().filter(midia -> midia.getAluguel() != null).collect(Collectors.toList());

                if (midiasAlugadas.size() >= midiasNaoAlugadas.size()) {
                    disponivel = false;
                    previsao = midiaService.findFirstByFilmeIdAndTipoOrderByAluguelDataPrevisaoAsc(midiasAlugadas.get(0).getFilme().getId(),
                            midiasAlugadas.get(0).getTipo()).getAluguel().getDataPrevisao();
                }

                infos.add(MidiaInfoDTO.builder().build().toMidiaInfoDTO(midiasVHS, disponivel, previsao, valor.getValor()));
            }

            if (!midiasDVD.isEmpty()) {
                ValorMidia valor = valorMidiaService.findByMidiaIdAndFimVigenciaIsNull(midiasDVD.get(0).getId());

                Boolean disponivel = true;
                
                LocalDateTime previsao = null;

                List<Midia> midiasNaoAlugadas = midiasDVD
                        .stream().filter(midia -> midia.getAluguel() == null).collect(Collectors.toList());

                List<Midia> midiasAlugadas = midiasDVD
                        .stream().filter(midia -> midia.getAluguel() != null).collect(Collectors.toList());

                if (midiasAlugadas.size() >= midiasNaoAlugadas.size()) {
                    disponivel = false;
                    previsao =  midiaService.findFirstByFilmeIdAndTipoOrderByAluguelDataPrevisaoAsc(midiasAlugadas.get(0).getFilme().getId(),
                               midiasAlugadas.get(0).getTipo()).getAluguel().getDataPrevisao();
                }

                infos.add(MidiaInfoDTO.builder().build().toMidiaInfoDTO(midiasDVD, disponivel, previsao, valor.getValor()));
            }

            if (!midiasBLUE.isEmpty()) {
                ValorMidia valor = valorMidiaService.findByMidiaIdAndFimVigenciaIsNull(midiasBLUE.get(0).getId());

                Boolean disponivel = true;

                LocalDateTime previsao = null;

                List<Midia> midiasNaoAlugadas = midiasBLUE
                        .stream().filter(midia -> midia.getAluguel() == null).collect(Collectors.toList());

                List<Midia> midiasAlugadas = midiasBLUE
                        .stream().filter(midia -> midia.getAluguel() != null).collect(Collectors.toList());

                if (midiasAlugadas.size() >= midiasNaoAlugadas.size()) {
                    disponivel = false;
                    previsao =  midiaService.findFirstByFilmeIdAndTipoOrderByAluguelDataPrevisaoAsc(midiasAlugadas.get(0).getFilme().getId(),
                               midiasAlugadas.get(0).getTipo()).getAluguel().getDataPrevisao();
                }

                infos.add(MidiaInfoDTO.builder().build().toMidiaInfoDTO(midiasBLUE, disponivel, previsao, valor.getValor()));
            }

            catalogo.add(CatalogoDTO.builder().build().toCalotoDTO(filme, infos));
        }
         
        return new PageImpl<>(catalogo, pageable, catalogo.size());
    }
}
