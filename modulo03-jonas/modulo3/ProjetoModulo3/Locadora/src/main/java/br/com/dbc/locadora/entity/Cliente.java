/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name="CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ", allocationSize = 1)
public class Cliente implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENTE_SEQ")
    private Long id;
    
    @Column(nullable = false)
    @Size(min = 1, max = 100)
    private String nome;
    
    private Long telefone;
   
    @Column(nullable = false)
    @Size(min = 1, max = 100)
    private String bairro;
    
    @Column(nullable = false)
    @Size(min = 1, max = 100)
    private String cidade;
    
    @Column(nullable = false)
    @Size(min = 1, max = 100)
    private String rua;
    
    @Column(nullable = false)
    private Long numero;
    
    @Column(nullable = false)
    @Size(min = 1, max = 100)
    private String estado;
    
    
}
