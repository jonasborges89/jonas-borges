/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.dto.ValorMidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jonas.borges
 */
@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia> {

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    MidiaService midiaService;

    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }

    @Transactional(readOnly = false)
    public void deleteByIdMidia(Midia midia) {
        valorMidiaRepository.deleteByMidiaId(midia.getId());
    }

    public Page<ValorMidiaDTO> findValoresByIdFilme(List<Midia> midias, Filme filme, Pageable pageable) {

        List<ValorMidiaDTO> valoresDTO = new ArrayList<>();

        ValorMidiaDTO valorMidiaDTO = ValorMidiaDTO.builder().build();

        for (Midia midia : midias) {
            ValorMidia valor = valorMidiaRepository.findByMidiaIdAndFimVigenciaIsNull(midia.getId());
            valoresDTO.add(valorMidiaDTO.ValorMidiaToDTO(valor, filme));
        }

        return new PageImpl<>(
                valoresDTO,
                pageable,
                valoresDTO.size());
    }

    public ValorMidia findByMidiaIdAndInicioVigenciaBetween(Long id, LocalDateTime retirada) {
        return valorMidiaRepository.findByMidiaIdAndInicioVigenciaBetween(id, retirada);
    }

    public ValorMidia findByMidiaIdAndFimVigenciaIsNull(Long id) {
        return valorMidiaRepository.findByMidiaIdAndFimVigenciaIsNull(id);
    }
    
    public List<ValorMidia> findByMidiaId(Long id){
        return valorMidiaRepository.findByMidiaId(id);
    }

    public void atualizaValoresMidia(Midia midia, MidiaDTO midiaDTO) {
        ValorMidia valorMidia = valorMidiaRepository.findByMidiaIdAndFimVigenciaIsNull(midia.getId());
        valorMidia.setFimVigencia(LocalDateTime.now());
        valorMidiaRepository.save(valorMidia);

        valorMidiaRepository.save(ValorMidia.builder().inicioVigencia(LocalDateTime.now())
                .valor(midiaDTO.getValor())
                .midia(midia).build());
    }

    public Double calculaValorAluguel(List<ValorMidia> valoresMidia) {
        Double totAluguel = 0.0;

        totAluguel = valoresMidia.stream().map((valorMidia) ->
                valorMidia.getValor()).reduce(totAluguel, (accumulator, _item)
                        -> accumulator + _item);

        return totAluguel;
    }

}
