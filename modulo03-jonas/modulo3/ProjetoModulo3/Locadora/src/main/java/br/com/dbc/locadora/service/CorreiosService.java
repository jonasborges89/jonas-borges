/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.ws.ConsultaCEP;
import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import br.com.dbc.locadora.ws.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.dto.ConsultaCEPDTO;
import javax.xml.bind.JAXBElement;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas.borges
 */
@Service
public class CorreiosService {

    private final String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";
    
    @Autowired
    private ObjectFactory objectFactory;

    @Autowired
    private SoapConnector soapConnector;

    public ConsultaCEPDTO buscaEnderecoPorCep(String cep) {
        
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);
        
        ConsultaCEPResponse resposta = ((JAXBElement<ConsultaCEPResponse>) 
                soapConnector.callWebService(url, objectFactory.createConsultaCEP(consulta))).getValue();
        
        return ConsultaCEPDTO.builder().build().toConsultaCEPDTO(resposta);
        
    }

}
