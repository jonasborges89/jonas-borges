/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatalogoDTO {

    private String titulo;

    private Categoria categoria;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate lancamento;

    private List<MidiaInfoDTO> midiaInfo;

    public CatalogoDTO toCalotoDTO(Filme filme, List<MidiaInfoDTO> midias) {

        this.setTitulo(filme.getTitulo());
        this.setCategoria(filme.getCategoria());
        this.setLancamento(filme.getLancamento());
        this.setMidiaInfo(midias);
        return this;
    }

}
