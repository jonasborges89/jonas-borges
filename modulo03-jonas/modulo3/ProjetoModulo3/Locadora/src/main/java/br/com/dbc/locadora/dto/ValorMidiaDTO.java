/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author jonas
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValorMidiaDTO {

    private Long id;
    
    private String titulo;

    private MidiaType tipo;
    
    private Double valor;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime inicioVigencia;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime fimVigencia;

    public ValorMidiaDTO ValorMidiaToDTO(ValorMidia valor, Filme filme) {
        return ValorMidiaDTO.builder()
                .id(valor.getMidia().getId())
                .titulo(filme.getTitulo())
                .tipo(valor.getMidia().getTipo())
                .valor(valor.getValor())
                .inicioVigencia(valor.getInicioVigencia())
                .fimVigencia(valor.getFimVigencia())
                .build();

    }

}
