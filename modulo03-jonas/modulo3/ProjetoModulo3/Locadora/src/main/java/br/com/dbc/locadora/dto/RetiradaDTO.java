/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Aluguel;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author jonas.borges
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RetiradaDTO {

    private Long id;

    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataRetirada;

    @JsonFormat(pattern = "dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataPrevisao;
    
    private Double valorPrevisto;

     public RetiradaDTO toRetiradaDTO(Aluguel aluguel, Double valor) {
        
        this.setId(aluguel.getId());
        this.setDataRetirada(aluguel.getDataRetirada().toLocalDate());
        this.setDataPrevisao(aluguel.getDataPrevisao().toLocalDate());
        this.setValorPrevisto(valor);
        
        return this;
    }
}
