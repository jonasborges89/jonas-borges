/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Categoria;
import org.springframework.data.domain.Pageable;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *
 * @author jonas.borges
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public Long countByFilmeIdAndTipo(Long idFilme, MidiaType tipo);
    
    public List<Midia> findByAluguelNotNull();
    
    public List<Midia> findByFilmeIdAndTipo(Long idFilme, MidiaType tipo);
    
    public List<Midia> findByFilmeId(Long idFilme);
    
    public Long countByTipo(MidiaType tipo);
    
    public List<Midia> findByAluguelId(Long id);
    
    public Midia findFirstByFilmeIdAndTipo(Long idFilme, MidiaType tipo);
   
    public Page<Midia> findByFilmeTituloContainingIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween(Pageable pageable,
            String titulo,
            Categoria categoria,
            LocalDate lancamentoIni,
            LocalDate lancamentoFim);
    
    public Midia findFirstByFilmeIdAndTipoOrderByAluguelDataPrevisaoAsc(Long id, MidiaType tipo);
    
}
