/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaCEPDTO {
    
    private String rua;
    
    private String bairro;
    
    private String cidade;
    
    private String estado;
    
    public ConsultaCEPDTO toConsultaCEPDTO(ConsultaCEPResponse resposta){
        this.setRua(resposta.getReturn().getEnd());
        this.setBairro(resposta.getReturn().getBairro());
        this.setCidade(resposta.getReturn().getCidade());
        this.setEstado(resposta.getReturn().getUf());
        return this;
        
    }
    
    
   
    
}
