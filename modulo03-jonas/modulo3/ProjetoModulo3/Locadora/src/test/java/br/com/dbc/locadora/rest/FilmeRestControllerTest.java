/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author jonas.borges
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    public FilmeRestControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Override
    protected AbstractRestController getController() {
        return filmeRestController;
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    public FilmeDTO criarFilmeComMidias() {
        return FilmeDTO.builder()
                .titulo("Titanic")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now())
                .midia(Arrays.asList(MidiaDTO.builder()
                        .tipo(MidiaType.VHS)
                        .quantidade(1)
                        .valor(1.9).build(), MidiaDTO.builder()
                        .tipo(MidiaType.DVD)
                        .quantidade(2)
                        .valor(0.9).build())).build();
    }

    @Test
    public void testFindByTituloOrCategoriaOrLancamento() throws Exception {
        Filme f = filmeRepository.save(Filme.builder().titulo("Titanic").categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now()).build());

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search?page=0&size=10&sort=id,desc&titulo={titulo}", f.getTitulo())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value(f.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value(f.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

    }

    /**
     * Test of SalvarFilmeComMidia method, of class FilmeRestController.
     */
    @Test
    public void testSalvarFilmeComMidia() throws Exception {

        FilmeDTO f = this.criarFilmeComMidias();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(f.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f.getLancamento()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        List<Filme> fList = filmeRepository.findAll();
        List<Midia> mList = midiaRepository.findAll();
        Assert.assertEquals(1, fList.size());
        Assert.assertEquals(3, mList.size());
    }

    /**
     * Test of countByIdFilmeAndTipo method, of class FilmeRestController.
     */
    @Test
    public void testCountByIdFilmeAndTipo() throws Exception {
        Filme filme = filmeRepository.save(Filme.builder().titulo("Titanic").categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now()).build());

        Midia midia = midiaRepository.save(Midia.builder()
                .tipo(MidiaType.VHS)
                .filme(filme)
                .build());

        Midia midia2 = midiaRepository.save(Midia.builder()
                .tipo(MidiaType.VHS)
                .filme(filme)
                .build());

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/count/{id}/{tipo}",
                filme.getId(), midia.getTipo())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").value("2"));
    }

    /*@Test
    public void testBuscarCatalogo() throws Exception {

        FilmeDTO f = this.criarFilmeComMidias();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].categoria").value(f.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].lancamento").value(f.getLancamento()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midiaInfo.[0].tipo").value(f.getMidia().get(0).getTipo().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].midiaInfo.[0].valor").value(f.getMidia().get(0).getValor()));
        
                 .andExpect(MockMvcResultMatchers.jsonPath("$.midiaInfo.[0].quantidade").value(f.getMidia().get(0).getQuantidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.midiaInfo.[0].disponivel").value(true))
                .andExpect(MockMvcResultMatchers.jsonPath("$.midiaInfo.[0].previsaoDisponibilidade").value(null));

       
    }*/
    
    @Test
    public void testUpdateFilme() throws Exception {

        FilmeDTO f = this.criarFilmeComMidias();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        f = FilmeDTO.builder()
                .id(1l)
                .titulo("Bilbo")
                .categoria(Categoria.ANIMACAO)
                .lancamento(LocalDate.now())
                .midia(Arrays.asList(MidiaDTO.builder()
                        .tipo(MidiaType.VHS)
                        .quantidade(3)
                        .valor(1.9).build(), MidiaDTO.builder()
                        .tipo(MidiaType.DVD)
                        .quantidade(2)
                        .valor(0.9).build())).build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/update/{id}", f.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(f)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(f.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(f.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(f.getLancamento()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

    }

}
