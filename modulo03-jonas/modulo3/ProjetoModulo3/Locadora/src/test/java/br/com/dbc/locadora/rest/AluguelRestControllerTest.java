/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author jonas
 */
public class AluguelRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private AluguelRestController aluguelRestController;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    public AluguelRestControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
        aluguelRepository.deleteAll();
        clienteRepository.deleteAll();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Override
    protected AbstractRestController getController() {
        return aluguelRestController;
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetService() {

    }

    @Test
    public void testAluguelRetirada() throws Exception {
         Cliente c = clienteRepository.save(Cliente.builder()
                .nome("nome")
                .bairro("Partenon")
                .cidade("Porto Alegre")
                .estado("RS")
                .rua("Bento Gonlçalves")
                .numero(2199l)
                .telefone(9999999l)
                .build());
        Filme filme = filmeRepository.save(Filme.builder().titulo("Matrix")
                .lancamento(LocalDate.now())
                .categoria(Categoria.AVENTURA).build());

        Midia midia = midiaRepository.save(Midia.builder().tipo(MidiaType.VHS).filme(filme).build());

        ValorMidia valor = valorMidiaRepository.save(ValorMidia.builder().inicioVigencia(LocalDateTime.now())
                .valor(2.0)
                .midia(midia).build());

        AluguelDTO aluguelDTO = AluguelDTO.builder().idCliente(c.getId()).midias(Arrays.asList(midia.getId())).build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataRetirada").value(LocalDate.now()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataPrevisao")
                        .value(LocalDate.now().plusDays(1).atTime(16, 0)
                                .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valorPrevisto").value(valor.getValor()));
    }

    @Test
    public void testAluguelDevolucao() throws JsonProcessingException, Exception {

        Cliente c = clienteRepository.save(Cliente.builder()
                .nome("nome")
                .bairro("Partenon")
                .cidade("Porto Alegre")
                .estado("RS")
                .rua("Bento Gonlçalves")
                .numero(2199l)
                .telefone(9999999l)
                .build());

        Filme filme = filmeRepository.save(Filme.builder().titulo("Matrix")
                .lancamento(LocalDate.now())
                .categoria(Categoria.AVENTURA).build());

        Midia midia = midiaRepository.save(Midia.builder().tipo(MidiaType.VHS).filme(filme).build());

        ValorMidia valor = valorMidiaRepository.save(ValorMidia.builder().inicioVigencia(LocalDateTime.now())
                .valor(2.0)
                .midia(midia).build());

        Aluguel aluguel = aluguelRepository.save(Aluguel.builder()
                .idCliente(c)
                .dataRetirada(LocalDateTime.now())
                .dataPrevisao(LocalDate.now().plusDays(1).atTime(16, 0))
                .dataEntrega(LocalDate.now().plusDays(1).atTime(17, 0))
                .build());

        AluguelDTO aluguelDTO = AluguelDTO.builder().midias(Arrays.asList(midia.getId())).build();

        midia.setAluguel(aluguel);
        midia = midiaRepository.save(midia);
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataRetirada").value(aluguel.getDataRetirada()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataPrevisao").value(aluguel.getDataPrevisao()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataEntrega").value(LocalDate.now()
                        .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.multa").value(0.0));
    }

    @Test
    public void testDevolucoesHoje() throws Exception {

          Cliente c = clienteRepository.save(Cliente.builder()
                .nome("nome")
                .bairro("Partenon")
                .cidade("Porto Alegre")
                .estado("RS")
                .rua("Bento Gonlçalves")
                .numero(2199l)
                .telefone(9999999l)
                .build());

        Filme filme = filmeRepository.save(Filme.builder().titulo("Matrix")
                .lancamento(LocalDate.now())
                .categoria(Categoria.AVENTURA).build());

        Midia midia = midiaRepository.save(Midia.builder().tipo(MidiaType.VHS).filme(filme).build());

        ValorMidia valor = valorMidiaRepository.save(ValorMidia.builder().inicioVigencia(LocalDateTime.now())
                .valor(2.0)
                .midia(midia).build());

        Aluguel aluguel = aluguelRepository.save(Aluguel.builder()
                .idCliente(c)
                .dataRetirada(LocalDate.now().atStartOfDay())
                .dataPrevisao(LocalDate.now().atTime(15, 0))
                .build());

        midia.setAluguel(aluguel);
        midia = midiaRepository.save(midia);
        
          restMockMvc.perform(MockMvcRequestBuilders.get("/api/aluguel/devolucao?page=0&size=10&sort=id,desc")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].titulo")
                        .value(filme.getTitulo()));
          
        
    }
}
