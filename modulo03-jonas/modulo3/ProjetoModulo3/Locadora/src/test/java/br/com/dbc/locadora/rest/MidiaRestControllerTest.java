/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.MidiaSearchDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author jonas
 */
public class MidiaRestControllerTest extends LocadoraApplicationTests {

    public MidiaRestControllerTest() {
    }

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private FilmeRepository filmeRepository;
    
    @Autowired
    private MidiaRestController midiaRestController;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }
    
    @Override
    protected AbstractRestController getController() {
        return midiaRestController;
    }

    /**
     * Test of countByTipo method, of class MidiaRestController.
     */
    @Test
    public void testCountByTipo() throws Exception {
        Filme filme = filmeRepository.save(Filme.builder().titulo("Matrix")
                .lancamento(LocalDate.now())
                .categoria(Categoria.AVENTURA).build());
        
        Midia midia = midiaRepository.save(Midia.builder()
                        .tipo(MidiaType.VHS)
                        .filme(filme)
                        .build());
        
         Midia midia2 = midiaRepository.save(Midia.builder()
                        .tipo(MidiaType.VHS)
                        .filme(filme)
                        .build());
         
           restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/count/{tipo}", "VHS")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
               .andExpect(MockMvcResultMatchers.jsonPath("$")
                       .value("2"));

    }

    /**
     * Test of findMidiaByTituloOrCategoriaOrLancamento method, of class
     * MidiaRestController.
     */
    @Test
    public void testFindMidiaByTituloOrCategoriaOrLancamento() throws Exception {

         Filme filme = filmeRepository.save(Filme.builder().titulo("Titanic").categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.now()).build());
         
         Midia midia = midiaRepository.save(Midia.builder()
                        .tipo(MidiaType.VHS)
                        .filme(filme)
                        .build());
         
          ValorMidia valor = valorMidiaRepository.save(ValorMidia.builder().inicioVigencia(LocalDateTime.now())
                .valor(2.0)
                .midia(midia).build());
         
         MidiaSearchDTO dtoMidia = MidiaSearchDTO.builder().build().toMidiaSearchDTO(filme, midia, valor);

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/"
                + "search?page=0&size=10&sort=id,desc&titulo={titulo}", filme.getTitulo())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].id").isNumber())
                
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].tipo")
                        .value(dtoMidia.getTipo().toString()))
                
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].valor")
                        .value(dtoMidia.getValor()))
                
                .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].filme.id")
                        .value(dtoMidia.getFilme().getId()))
                
                 .andExpect(MockMvcResultMatchers.jsonPath("$.content.[0].filme.titulo")
                        .value(dtoMidia.getFilme().getTitulo()));
    }
}
