/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity.exercicio;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author tiago
 */
@Entity
@Table(name = "USUARIO_SISTEMA")
public class UsuarioSistema implements Serializable {

    @Id
    @SequenceGenerator(name = "USUARIO_SISTEMA_SEQ", sequenceName = "USUARIO_SISTEMA_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "USUARIO_SISTEMA_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    @JoinColumn(name = "ID_SISTEMA", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Sistema idSistema;
    @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public UsuarioSistema() {
    }

    public UsuarioSistema(Sistema idSistema, Usuario idUsuario) {
        this.idSistema = idSistema;
        this.idUsuario = idUsuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sistema getIdSistema() {
        return idSistema;
    }

    public void setIdSistema(Sistema idSistema) {
        this.idSistema = idSistema;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioSistema)) {
            return false;
        }
        UsuarioSistema other = (UsuarioSistema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.dbc.petshop.entity.exercicio.UsuarioSistema[ id=" + id + " ]";
    }
    
}
