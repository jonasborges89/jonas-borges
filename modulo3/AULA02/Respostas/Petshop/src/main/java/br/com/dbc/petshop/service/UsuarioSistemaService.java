/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.exercicio.Sistema;
import br.com.dbc.petshop.entity.exercicio.Usuario;
import br.com.dbc.petshop.entity.exercicio.UsuarioSistema;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author tiago
 */
public class UsuarioSistemaService {

    private static final UsuarioSistemaService instance;

    static {
        instance = new UsuarioSistemaService();
    }

    public static UsuarioSistemaService getInstance() {
        return instance;
    }

    private UsuarioSistemaService() {
    }

    public List<UsuarioSistema> findAll() {
        EntityManager em = PersistenceUtils.getEm();
        List<UsuarioSistema> usuarioSistemas = em.createQuery("select a from UsuarioSistema a").getResultList();
        return usuarioSistemas;
    }

    public List<UsuarioSistema> findAllCriteria() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(UsuarioSistema.class).list();
    }

    public UsuarioSistema create(UsuarioSistema c) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            c = em.merge(c);
            em.getTransaction().commit();
            return c;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public void batchCreate(List<UsuarioSistema> usuarioSistemas) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            usuarioSistemas.forEach(em::persist);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public UsuarioSistema createCriteria(UsuarioSistema c) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.save(c);
            t.commit();
            return c;
        } catch (Exception e) {
            t.rollback();
            throw e;
        }
    }

    public List<UsuarioSistema> vinculaUsuariosComSistema(Sistema sistema, List<Usuario> usuarios) {
        List<UsuarioSistema> usuarioSistemas = new ArrayList<>();

        usuarios.forEach(u->{
            usuarioSistemas.add(new UsuarioSistema(sistema, u));
        });
        
        batchCreate(usuarioSistemas);
        
        return usuarioSistemas;
    }
    
}
