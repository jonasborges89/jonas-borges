/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.exercicio.Usuario;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author tiago
 */
public class UsuarioService {

    private static final UsuarioService instance;

    static {
        instance = new UsuarioService();
    }

    public static UsuarioService getInstance() {
        return instance;
    }

    private UsuarioService() {
    }

    public List<Usuario> findAll() {
        EntityManager em = PersistenceUtils.getEm();
        List<Usuario> usuarios = em.createQuery("select a from Usuario a").getResultList();
        return usuarios;
    }

    public List<Usuario> findAllCriteria() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Usuario.class).list();
    }

    public Usuario create(Usuario c) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            c = em.merge(c);
            em.getTransaction().commit();
            return c;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public void batchCreate(List<Usuario> usuarios) {
        EntityManager em = PersistenceUtils.getEm();
        try {
            em.getTransaction().begin();
            usuarios.forEach(em::persist);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        }
    }

    public Usuario createCriteria(Usuario c) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        try {
            session.save(c);
            t.commit();
            return c;
        } catch (Exception e) {
            t.rollback();
            throw e;
        }
    }

    public List<Usuario> insere10Usuarios() {
        List<Usuario> usuarios = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            usuarios.add(new Usuario("Usuario" + i, new Date()));
        }

        batchCreate(usuarios);
        
        return usuarios;
    }

}
