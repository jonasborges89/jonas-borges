/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.seguradora.rest.converter;

import br.com.dbc.seguradora.entity.Cliente;
import br.com.dbc.seguradora.rest.dto.ClienteDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 *
 * @author tiago
 */
@Component
public class ClienteDTOConverter implements Converter<ClienteDTO, Cliente> {

    @Override
    public Cliente convert(ClienteDTO input) {
        return Cliente
                .builder()
                .nome(input.getPrimeiroNome() + " " + input.getUltimoNome())
                .build();
    }

}
