﻿using RestWithAspNet2.Model;
using System.Collections.Generic;

namespace RestWithAspNet2.Services.Implementattions
{
    public interface IPersonService
    {
        Person Create(Person person);
        Person FindById(long id);
        List<Person> FindAll();
        Person Update(Person person);
        void Delete(long id);
    }
}
