import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    private final double DELTA = 0.1;
    
    @After
    public void tearDown(){
        System.gc();  
    }

    @Test
    public void elfoNoturnoAtiraDuasFlechasGanha6ExperienciaEPerde15DeVida(){
        ElfoNoturno galandriel = new ElfoNoturno("Galandriel");
        galandriel.atirarFlecha(new Dwarf("Dunga"));
        galandriel.atirarFlecha(new Dwarf("Dunga"));
        assertEquals(70,galandriel.getVida(), DELTA);
        assertEquals(6,galandriel.getExperiencia(), DELTA);
    }
}
