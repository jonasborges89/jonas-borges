import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest
{
    @After
    public void tearDown(){
        System.gc();  
    }

    @Test
    public void adicionadoItensInventario(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        assertEquals(arco,inventario.obter(0));
        assertEquals(flecha,inventario.obter(1));
    }

    @Test
    public void obtendoItemPorPosicao(){
        Item varinhaMagica = new Item("Varinha Mágica do Harry Potter", 1);
        Inventario inventario = new Inventario();
        inventario.adicionar(varinhaMagica);
        assertEquals(varinhaMagica, inventario.obter(0));
    }

    @Test
    public void exlcusaoDeItem(){
        Item sabreDeLuz = new Item("Sabre de luz", 2);
        Inventario inventario = new Inventario();
        inventario.adicionar(sabreDeLuz);
        assertEquals(sabreDeLuz, inventario.obter(0));
        inventario.remover(0);
        assertEquals(0, inventario.getTamanhoInventario());
    }

    @Test 
    public void retornoDescricaoItens(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item varinhaMagica = new Item("Varinha Mágica do Harry Potter", 1);
        Item escudoAdamantium = new Item("Escudo de Adamantium", 1);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(varinhaMagica);
        inventario.adicionar(escudoAdamantium);
        inventario.remover(2);
        inventario.remover(0);
        assertEquals("Flecha,Escudo de Adamantium", inventario.getDescricoesItens());
    }

    @Test
    public void buscaItemMaiorQuantidade(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item varinhaMagica = new Item("Varinha Mágica do Harry Potter", 2);
        Item escudoAdamantium = new Item("Escudo de Adamantium", 10);
        Item espada = new Item("Espada", 8);
        Item martelo = new Item("Martelo", 3);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(varinhaMagica);
        inventario.adicionar(escudoAdamantium);
        inventario.adicionar(espada);
        inventario.adicionar(martelo);
        assertEquals(escudoAdamantium, inventario.getItemMaiorQtd());
    }

    @Test 
    public void buscaItemNoInventarioPorNome(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        Item testaBuscaEscudo = inventario.getItemPorNome("Escudo");
        Item testaBuscaFlecha = inventario.getItemPorNome("Flecha");
        Item testaBuscaArco = inventario.getItemPorNome("Arco");
        assertEquals(escudo,testaBuscaEscudo);
        assertEquals(flecha,testaBuscaFlecha);
        assertEquals(arco,testaBuscaArco);
    }

    @Test
    public void retornaArrayInvertido(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Item martelo = new Item("Martelo",2);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        Inventario invertido = new Inventario();
        invertido.adicionar(martelo);
        invertido.adicionar(escudo);
        invertido.adicionar(flecha);
        invertido.adicionar(arco);
        assertEquals(invertido.getItens(), inventario.inverter());
    }

    @Test
    public void reordenarInventario(){
        Item arco = new Item("Arco", 3);
        Item flecha = new Item("Flecha", 2);
        Item escudo = new Item("Escudo",1);
        Item capacete = new Item("Capacete", 7);
        Item lanca = new Item("Lança",5);
        Item adaga = new Item("Adaga",4);

        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(capacete);
        inventario.adicionar(lanca);
        inventario.adicionar(adaga);

        Inventario inventarioParaComparacao = new Inventario();
        inventarioParaComparacao.adicionar(escudo);
        inventarioParaComparacao.adicionar(flecha);
        inventarioParaComparacao.adicionar(arco);
        inventarioParaComparacao.adicionar(adaga);
        inventarioParaComparacao.adicionar(lanca);
        inventarioParaComparacao.adicionar(capacete);

        inventario.ordenarItens();
        assertEquals(inventarioParaComparacao.getItens(),inventario.getItens());
    }
    
    @Test 
    public void buscaItemNoInventarioPorNomeComDoisItensIguais(){
        Item arco = new Item("Arco", 45);
        Item arco2 = new Item("Arco", 7);
        Item arco3 = new Item("Arco",2);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(arco2);
        inventario.adicionar(arco3);
        assertEquals(arco,inventario.getItemPorNome("Arco"));
    }
    
    @Test
    public void inventarioCheioSemOItemBuscado(){
        Item arco = new Item("Arco", 3);
        Item flecha = new Item("Flecha", 2);
        Item escudo = new Item("Escudo",1);
        Item capacete = new Item("Capacete", 7);
        Item lanca = new Item("Lança",5);
        Item adaga = new Item("Adaga",4);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(capacete);
        inventario.adicionar(lanca);
        inventario.adicionar(adaga);
        assertEquals(null,inventario.getItemPorNome("Machado"));
    }
}
