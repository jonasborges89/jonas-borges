import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void calculaMediaItensNoInventario(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Item martelo = new Item("Martelo",2);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3,estatisticas.getCalculoMediaQtdItens(), 0.1);
    }

    @Test
    public void calculaMedianaImpar(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",30);
        Item martelo = new Item("Martelo",2);
        Item capacete = new Item("Martelo",1);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        inventario.adicionar(capacete);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2, estatisticas.getCalculoMediana(), 0.1);
    }

    @Test
    public void calculaMedianaPar(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Item martelo = new Item("Martelo",3);
        Item capacete = new Item("Capacete",10);
        Item luva = new Item("Luva de Mithrandir",2);
        Item botas = new Item("Botas Voadoras ",1);
        Item capa = new Item("Capa de Invisibilidade",1); 

        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        inventario.adicionar(capacete);
        inventario.adicionar(luva);
        inventario.adicionar(botas);
        inventario.adicionar(capa);

        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2, estatisticas.getCalculoMediana(), 0.1);
    }

    @Test
    public void calculaItensAcimaDaMedia(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Item martelo = new Item("Martelo",4);
        Item capacete = new Item("Capacete",10);
        Item luva = new Item("Luva de Mithrandir",2);
        Item botas = new Item("Botas Voadoras ",1);
        Item capa = new Item("Capa de Invisibilidade",1);
        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        inventario.adicionar(capacete);
        inventario.adicionar(luva);
        inventario.adicionar(botas);
        inventario.adicionar(capa);
        
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.getQtdItensAcimaDaMedia());
    }
}
