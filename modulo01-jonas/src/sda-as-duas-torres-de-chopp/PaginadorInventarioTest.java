import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void getInventarioPaginado(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Item martelo = new Item("Martelo",3);
        Item capacete = new Item("Capacete",10);
        Item luva = new Item("Luva de Mithrandir",2);

        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        inventario.adicionar(capacete);
        inventario.adicionar(luva);

        Inventario inventarioParaComparacao = new Inventario();
        inventarioParaComparacao.adicionar(arco);
        inventarioParaComparacao.adicionar(flecha);

        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        assertEquals(inventarioParaComparacao.getItens(), paginador.limitar(2));       
    }

    @Test
    public void getInventarioPaginadoCom4pulos(){
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        Item escudo = new Item("Escudo",2);
        Item martelo = new Item("Martelo",3);
        Item capacete = new Item("Capacete",10);
        Item luva = new Item("Luva de Mithrandir",2);

        Inventario inventario = new Inventario();
        inventario.adicionar(arco);
        inventario.adicionar(flecha);
        inventario.adicionar(escudo);
        inventario.adicionar(martelo);
        inventario.adicionar(capacete);
        inventario.adicionar(luva);

        Inventario inventarioParaComparacao = new Inventario();
        inventarioParaComparacao.adicionar(capacete);
        inventarioParaComparacao.adicionar(luva);

        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(4);
        assertEquals(inventarioParaComparacao.getItens(), paginador.limitar(2));       
    }
}
