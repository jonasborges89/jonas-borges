import java.util.ArrayList;
import java.util.Arrays;
public class ElfoDaLuz extends Elfo{

    private int qtdAtaques = 0;
    private static final double QTD_VIDA = 10.0;
    private static final ArrayList<String> DESCRICAO_ITENS_OBRIGATORIO = new ArrayList<>(Arrays.asList(new String[]
                {"Espada de aço valiriano"}));

    {
        ItemImperdivel espadaGalvorn = new ItemImperdivel("Espada de Galvorn", 1);
        super.ganharItem(espadaGalvorn);
        this.QTD_DANO = 21.0;
    }

    public ElfoDaLuz(String nome) {
        super(nome);
    }

    public Item getEspadaGalvorn(){
        Item espadaGalvorn = this.getInventario().getItemPorNome("Espada de Galvorn");
        return espadaGalvorn;
    }

    private void ganharVida(){
        this.setVida(this.getVida() + this.QTD_VIDA);
    }

    public void ataqueComEspada(Dwarf dwarf){
        Item espada = this.getEspadaGalvorn();
        if(espada.getQuantidade() > 0){
            this.qtdAtaques ++;
            dwarf.perderVida();
            if(this.qtdAtaques % 2 != 0){
                this.perderVida();
            } else {
                this.ganharVida();
            }
        }
    }

    @Override
    protected void perderItem(Item item){
        if(!DESCRICAO_ITENS_OBRIGATORIO.contains(item.getDescricao())){
            this.inventario.getItens().remove(item);
        }   
    }
}