import java.util.ArrayList;
public class PaginadorInventario{

    private Inventario inventario;
    private int marcador;

    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }

    public void pular (int marcador){
        this.marcador = marcador;
    }
    
    public ArrayList<Item> limitar(int limitador){
        ArrayList<Item> itensRetorno = new ArrayList<>();
        int posicaoArray = 0 + this.marcador;
        for(int i = 0 ; i < limitador; i++){
           itensRetorno.add(this.inventario.obter(i + posicaoArray));
        }
        return itensRetorno;
    }
}