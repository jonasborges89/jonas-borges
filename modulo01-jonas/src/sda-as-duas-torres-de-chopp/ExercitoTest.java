import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ExercitoTest
{
    
     @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void exercitoCom3ElfosMortos(){
        ElfoVerde legolas = new ElfoVerde("Legolas");
        ElfoVerde elrond = new ElfoVerde("Elrond");
        ElfoNoturno feanor = new ElfoNoturno("Fëanor");
        ElfoNoturno galandriel = new ElfoNoturno("Galandriel");
        Exercito exercito = new Exercito();        
        legolas.setStatus(Status.MORTO);
        elrond.setStatus(Status.MORTO);
        feanor.setStatus(Status.MORTO);
        exercito.alistaElfo(legolas);
        exercito.alistaElfo(elrond);
        exercito.alistaElfo(feanor);
        exercito.alistaElfo(galandriel);
        ArrayList<Elfo> esperado = new ArrayList<>(Arrays.asList(legolas,elrond,feanor));
        assertEquals(esperado, exercito.getElfosPorStatus(Status.MORTO));
    }

    @Test
    public void tentaAlistarExercitoDeElfosEElfosDeLuz(){
        Elfo legolas = new Elfo("Legolas");
        ElfoDaLuz elrond = new ElfoDaLuz("Elrond");
        Exercito exercito = new Exercito(); 
        exercito.alistaElfo(legolas);
        exercito.alistaElfo(elrond);
        assertEquals(null, exercito.getElfosPorStatus(Status.VIVO));
        assertEquals(null, exercito.getElfosPorStatus(Status.MORTO));
    }
    
    @Test
    public void exercitoCom4Elfos(){
        ElfoVerde legolas = new ElfoVerde("Legolas");
        ElfoVerde elrond = new ElfoVerde("Elrond");
        ElfoNoturno feanor = new ElfoNoturno("Fëanor");
        ElfoNoturno galandriel = new ElfoNoturno("Galandriel");
        Elfo Balin = new Elfo("Balin");
        Exercito exercito = new Exercito();        
        exercito.alistaElfo(legolas);
        exercito.alistaElfo(elrond);
        exercito.alistaElfo(feanor);
        exercito.alistaElfo(galandriel);
        exercito.alistaElfo(Balin);
        ArrayList<Elfo> esperado = new ArrayList<>(Arrays.asList(legolas,elrond,feanor,galandriel));
        assertEquals(esperado, exercito.getElfosPorStatus(Status.VIVO));
    }
}
