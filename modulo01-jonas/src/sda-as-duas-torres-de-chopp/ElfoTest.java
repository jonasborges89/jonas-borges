import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class ElfoTest
{
     private final double DELTA = 0.1;
     
    @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void nascemZeroElfos(){
        /* Dwarf dwarf = new Dwarf("Balin");
         Dwarf dwarf2 = new Dwarf("Goblin");
         assertEquals(0, Elfo.getQtdElfosNascidos(), DELTA);*/
    }
    
    @Test
    public void nascemDoisElfos(){
    /*Elfo legolas = new Elfo("Legolas");
    Elfo celeborn = new Elfo("Celeborn");
    assertEquals(2, Elfo.getQtdElfosNascidos(), DELTA);*/
    }
   
    @Test
    public void criarElfoInformandoNome() {
        Elfo legolas = new Elfo("Legolas");
        assertEquals("Legolas", legolas.getNome());
    }   

    @Test
    public void celebornAtiraUmaFlecha() {
        Elfo celeborn = new Elfo("Celeborn");
        celeborn.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(6, celeborn.getInventario().obter(1).getQuantidade());
        assertEquals(1, celeborn.getExperiencia());
    }

    @Test
    public void legolasTentaAtirarOitoFlechas() {
        Elfo elfo = new Elfo("Legolas");
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(0, elfo.getInventario().obter(1).getQuantidade());
        assertEquals(7, elfo.getExperiencia());
    }

    @Test 
    public void elfoAtiraInumerasFlechasEMataDwarf(){
        Elfo elrond = new Elfo("Elrond");
        Dwarf balin = new Dwarf("Balin");
        for(int i = 0; i < 15; i++){
            elrond.atirarFlecha(balin);
            if(elrond.getInventario().obter(1).getQuantidade() == 0){
                elrond.getMoreItens(1);
            }
        }
        assertEquals(0, balin.getVida(),0.1);
        assertEquals(Status.MORTO, balin.getStatus());
    }

    @Test
    public void elfoPerdeUmItem(){
        Elfo legolas = new Elfo("Legolas");
        Item martelo = new Item("Martelo", 1);
        ArrayList<Item> itensParaComparacao = new ArrayList<>();
        itensParaComparacao.add(legolas.getInventario().obter(0));
        itensParaComparacao.add(legolas.getInventario().obter(1));
        legolas.ganharItem(martelo);
        legolas.getInventario();
        legolas.perderItem(martelo);
        assertEquals(itensParaComparacao, legolas.getInventario().getItens());
    }

    @Test 
    public void inventarioVazio(){
        Elfo legolas = new Elfo("Legolas");
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        legolas.perderItem(arco);
        legolas.perderItem(flecha);
        assertTrue(null , legolas.getInventario().isEmpty());
    }

    @Test
    public void inventarioComZeroItens(){
        Elfo legolas = new Elfo("Legolas");
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        legolas.perderItem(arco);
        legolas.perderItem(flecha);
        assertEquals(0 , legolas.getInventario().getTamanhoInventario());
    }
}