import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Inventario {

    private ArrayList<Item> itens;
    private TipoOrdenacao tipoOrdenacao;
    
    public Inventario(){
        this.itens = new ArrayList<>();   
    }

    public int getTamanhoInventario(){
        return this.itens.size();
    }

    public boolean isEmpty(){
        if(this.itens.size() == 0){
            return true; 
        }
        return false;
    }

    public void adicionar(Item item){
        this.itens.add(item);
    }

    public Item obter(int posicao){
        return this.itens.get(posicao);
    }

    public void remover(int posicao){
        this.itens.remove(posicao);
    }

    public String getDescricoesItens(){

        String descricaoItens = "";
        //Ver classe StringBuilder;
        for( Item item : this.itens){
            descricaoItens += "" + item.getDescricao() + ",";
        }

        descricaoItens = descricaoItens.substring (0, descricaoItens.length() - 1);
        return descricaoItens;
    }

    public Item getItemMaiorQtd(){
        int itemMaiorQtd = 0;

        Item itemComMaiorQuantidade = new Item("",0);

        for(Item item : this.itens){
            if(item == null){
                continue;
            }
            if(item.getQuantidade() > itemMaiorQtd){
                itemComMaiorQuantidade = item;
                itemMaiorQtd = item.getQuantidade();
            }
        }

        return itemComMaiorQuantidade;
    }

    public ArrayList<Item> getItens(){
        return this.itens;
    }

    public Item getItemPorNome(String nome){

        Item itemRetorno = null;

        for(Item item : this.itens){
            if(item.getDescricao().equals(nome)){
                return item;
            }
        }
        return itemRetorno;
    }

    public ArrayList<Item> inverter(){

        ArrayList<Item> invertido = new ArrayList<>();

        for(int i = this.itens.size() - 1; i >= 0; i--){
            invertido.add(this.itens.get(i));
        }

        return invertido;
    }
    
     public void ordenarItens() {
        ordenarItens(TipoOrdenacao.ASC);
    }

    public void ordenarItens(TipoOrdenacao tipoOrdenacao){
        Collections.sort(this.itens, new Comparator<Item>() {
                public int compare(Item item1, Item item2) {
                    int quantidade1 = item1.getQuantidade();
                    int quantidade2 = item2.getQuantidade();
                    return tipoOrdenacao == TipoOrdenacao.ASC ? 
                        Integer.compare(quantidade1, quantidade2) :
                    Integer.compare(quantidade2, quantidade1);
                }
            });
    }
}