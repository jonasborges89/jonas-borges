public abstract class Personagem {
    protected String nome;
    protected double vida;
    protected Status status;
    protected Inventario inventario;
    protected double QTD_DANO;

    protected Personagem(String nome, double vida, Status status){
        this.nome = nome;
        this.vida = vida;
        this.status = status;
        this.inventario = new Inventario();
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    protected void setStatus(Status status) {
        this.status = status;
    }

    public String getNome() {
        return this.nome;
    }

    public double getVida() {
        return this.vida;
    }

    public Status getStatus() {
        return this.status;
    }

    public Inventario getInventario(){
        return this.inventario;
    }

    public void ganharItem(Item item){
        this.inventario.adicionar(item);
    }

    protected void perderItem(Item item){
        this.inventario.getItens().remove(item);
    }

    public void getMoreItens(int posicao){
        this.inventario.obter(posicao).getMoreItens();
    }

    public void perderVida(){
        if(this.vida > 0){
            this.vida -= this.QTD_DANO;
            if(this.vida == 0){
                this.status = Status.MORTO; 
            }
        }
    }
}
