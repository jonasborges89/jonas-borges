import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;

public class Exercito
{
    private  HashMap<Status, ArrayList<Elfo>> exercitoElfos = new HashMap<>();
    ArrayList<Class> tiposPermitidos = new ArrayList(Arrays.asList(ElfoVerde.class, ElfoNoturno.class));

    public void alistaElfo(Elfo elfo){
        boolean podeAlistar = tiposPermitidos.contains(elfo.getClass());
        if(podeAlistar){
            ArrayList<Elfo> elfos = exercitoElfos.get(elfo.getStatus());
            if(elfos == null){
                elfos = new ArrayList<>();
                this.exercitoElfos.put(elfo.getStatus(),elfos);
            } 
            elfos.add(elfo);
        }
    }

    public ArrayList<Elfo> getElfosPorStatus(Status status){
        return this.exercitoElfos.get(status);
    }
}
