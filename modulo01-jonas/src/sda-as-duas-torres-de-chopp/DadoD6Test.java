import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class DadoD6Test
{
    @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void testeNumeroAleatorioEntre1e6(){
    ArrayList<Integer> permitidos = new ArrayList<>(Arrays.asList(1, 2, 3 , 4, 5, 6));
    DadoD6 dado = new DadoD6();
    int aleatorio = dado.sortear();
    assertTrue(permitidos.contains(aleatorio));
    }
}