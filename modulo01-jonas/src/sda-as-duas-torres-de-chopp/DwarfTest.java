import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void anaoPerde10DeVida(){
         Dwarf dwarf = new Dwarf("Jonas");
         dwarf.perderVida();
         assertEquals(100.00, dwarf.getVida(),0.1);
    }
    
    @Test
    public void dwarfNasceComVida(){
        Dwarf dwarf = new Dwarf("Balin");
        assertEquals(Status.VIVO, dwarf.getStatus());
    }
}
