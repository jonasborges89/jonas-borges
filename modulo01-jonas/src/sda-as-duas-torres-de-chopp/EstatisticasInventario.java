import java.util.ArrayList;
public class EstatisticasInventario
{
    private Inventario inventario;

    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }

    public Inventario getInventario(){
        return this.inventario;
    }

    public double getCalculoMediaQtdItens(){
        double qtdPorItem = 0, qtdItens = 0, media = 0;
        //ver Double.Nan//
        for(Item item : this.inventario.getItens()){
            qtdPorItem += item.getQuantidade();
            qtdItens += 1;
        }
        
        media = qtdPorItem/qtdItens;
        
        return media;
    }

    public double getCalculoMediana(){
        double mediana = 0.0;
        
        this.inventario.ordenarItens();
        
        if(this.inventario.getTamanhoInventario() % 2 != 0){
           int posicaoCentral = ((this.inventario.getTamanhoInventario() + 1) / 2) - 1;
           mediana = this.inventario.obter(posicaoCentral).getQuantidade();
           return mediana;
        }
        
        int posicao = (this.inventario.getTamanhoInventario() / 2) - 1;
        double valorPosicao1 = this.inventario.obter(posicao).getQuantidade();
        double valorPosicao2 = this.inventario.obter(posicao + 1).getQuantidade();    
        mediana = (valorPosicao1 + valorPosicao2) / 2;
        
        return mediana;
    }

    public int getQtdItensAcimaDaMedia(){
        double media = this.getCalculoMediaQtdItens();
        int qtdItensAcimaMedia = 0;
        
        for(Item item : this.inventario.getItens()){
            double qtdItens = (double) item.getQuantidade();
            if(qtdItens > media){
              qtdItensAcimaMedia += 1;
            }
        }
        return qtdItensAcimaMedia;
    }
}
