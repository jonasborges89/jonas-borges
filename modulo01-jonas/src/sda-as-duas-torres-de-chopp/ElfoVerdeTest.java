import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerdeTest
{
    
    @After
    public void tearDown(){
        System.gc();  
    }
    
    @Test
    public void elfoVerdeGanhaDobroExperiencia(){
        Elfo legolas = new Elfo("Legolas");
        ElfoVerde glorfindel = new ElfoVerde("Glorfindel");
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        legolas.atirarFlecha(new Dwarf("Gimli"));
        glorfindel.atirarFlecha(new Dwarf("Gimli"));
        glorfindel.atirarFlecha(new Dwarf("Gimli"));
        glorfindel.atirarFlecha(new Dwarf("Gimli"));
        glorfindel.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(4, legolas.getExperiencia());
        assertEquals(8, glorfindel.getExperiencia());
    }

    @Test
    public void recebeEspadaDeAcoValerianoEOcarinaDoTempo(){
        ElfoVerde lirazel = new ElfoVerde("Lirazel");
        Item espadaValeriana = new Item("Espada de aço valiriano",1);
        Item ocarinaDotempo = new Item("Ocarina do Tempo",1);
        lirazel.ganharItem(espadaValeriana);
        lirazel.ganharItem(ocarinaDotempo);
        Inventario invetarioParaComparacao = new Inventario();
        invetarioParaComparacao.adicionar(lirazel.getInventario().obter(0));
        invetarioParaComparacao.adicionar(lirazel.getInventario().obter(1));
        invetarioParaComparacao.adicionar(espadaValeriana);
        assertEquals(invetarioParaComparacao.getItens(), lirazel.getInventario().getItens());
    }
    
     public void recebeArcoDeVidroEOcarinaDoTempo(){
        ElfoVerde lirazel = new ElfoVerde("Lirazel");
        Item arcoDeVidro = new Item("Arco de Vidro",1);
        Item ocarinaDotempo = new Item("Ocarina do Tempo",1);
        lirazel.ganharItem(arcoDeVidro);
        lirazel.ganharItem(ocarinaDotempo);
        Inventario invetarioParaComparacao = new Inventario();
        invetarioParaComparacao.adicionar(lirazel.getInventario().obter(0));
        invetarioParaComparacao.adicionar(lirazel.getInventario().obter(1));
        invetarioParaComparacao.adicionar(arcoDeVidro);
        assertEquals(invetarioParaComparacao.getItens(), lirazel.getInventario().getItens());
    }

}
