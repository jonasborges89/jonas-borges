public class Elfo extends Personagem {

    protected String nome;
    protected int experiencia;
    protected int xpPorAtaque;
    private static int QtdElfosNascidos = 0;

    // type initializer
    {
        experiencia = 0;
        this.QTD_DANO = 0;
        Item arco = new Item("Arco", 1);
        Item flecha = new Item("Flecha", 7);
        super.ganharItem(arco);
        super.ganharItem(flecha);       
    }

    public Elfo(String nome) {
        super(nome, 100.0, Status.VIVO);
        this.xpPorAtaque = 1;
        Elfo.QtdElfosNascidos ++;
    }
    
    public void finalize() throws Throwable{
        Elfo.QtdElfosNascidos --;    
    }

    public static int getQtdElfosNascidos(){
        return Elfo.QtdElfosNascidos;
    }

    public void atirarFlecha(Dwarf dwarf) {
        Item flecha = this.inventario.obter(1);
        if (flecha.getQuantidade() > 0) {
            flecha.setQuantidade(flecha.getQuantidade() - 1);
            dwarf.perderVida();
            experiencia += this.xpPorAtaque;
            this.perderVida();
        } 
    }
    
    public static int getQtdElfos() {
        return QtdElfosNascidos;
    }

    public int getExperiencia() {
        return this.experiencia;
    }   
}