import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo {
    
    private final ArrayList<String> itensEspecificos = new ArrayList<>(Arrays.asList 
    (new String[]{"Espada de aço valiriano", "Arco de Vidro", "Flecha de Vidro"}));

    public ElfoVerde(String nome) {
        super(nome);
        this.xpPorAtaque = 2;
    }

    public void ganharItem(Item item){
      boolean descricaoValida = itensEspecificos.contains(item.getDescricao());
      if(descricaoValida){
        this.inventario.adicionar(item);
      }
    }
}
