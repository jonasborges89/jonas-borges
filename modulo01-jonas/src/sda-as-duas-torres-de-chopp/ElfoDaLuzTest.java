import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    private static final double DELTA = 0.1;
    
    @After
    public void tearDown(){
        System.gc();  
    }

    @Test
    public void testarDanosPorAtaque(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.ataqueComEspada(new Dwarf("Farlum")); 
        assertEquals(79.0, feanor.getVida(), DELTA);
        feanor.ataqueComEspada(new Dwarf("Gul"));
        assertEquals(89.0, feanor.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzSoAtacaComUnidadeDeEspada(){
        ElfoDaLuz legolas = new ElfoDaLuz("Legolas");
        legolas.getInventario().getItens().get(2).setQuantidade(0);
        legolas.ataqueComEspada(new Dwarf("Balin"));
        assertEquals(79, legolas.getVida(), DELTA);

    }
}
