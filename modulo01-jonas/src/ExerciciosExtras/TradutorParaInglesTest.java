import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TradutorParaInglesTest
{
    @Test
    public void testaTraducaoObrigado(){
        TradutorParaIngles tradutorBr = new TradutorParaIngles();
        assertEquals("Thank you", tradutorBr.traduzir("Obrigado"));
    }

}
