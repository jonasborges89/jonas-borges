import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumerosTest
{
    @Test
    public void verificaMediaSeguinteDoArray(){
        double[] arrayComparacao = new double[]{2.0, 4.0, 3.0, -4.5 };
        double[] entrada = new double[] { 1.0, 3.0, 5.0, 1.0, -10.0 };
        Numeros numeros = new Numeros(entrada);
        assertArrayEquals(arrayComparacao,numeros.calcularMediaSeguinte(), 0.1);
    }
}