import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumeroTest
{
    @Test
    public void verificarNumero5Impar(){
        Numero numero = new Numero(5);
        assertEquals(true,numero.impar());
    }

    @Test
    public void verifica27impar(){
        Numero numero =  new Numero(27);
        assertTrue(numero.impar());
    }
    
    @Test
    public void verifica2018impar(){
        Numero numero =  new Numero(2018);
        assertFalse(numero.impar());
    }
    

    @Test
    public void verificaSomaDivisivel(){
        Numero n = new Numero(3);
        assertEquals(false,n.verificarSomaDivisivel(17));
    }
}