public class FormaGeometricaFactory
{
    public static Retangulo criar(int tipo, int x, int y){
        Retangulo formaGeometrica = new Retangulo();
        switch (tipo){        
            case 1:
              formaGeometrica = new Retangulo();
              break;
              
            case 2:
              formaGeometrica = new Quadrado();
              break;
        }
        
        formaGeometrica.setX(x);
        formaGeometrica.setY(y);
        return formaGeometrica;        
    }   
}
