import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void retornaChavePorValor(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.consultar("Bernardo"); 
        assertEquals("Bernardo", agenda.consultarPorNumero("555555"));
        assertEquals("Mithrandir", agenda.consultarPorNumero("444444"));
    }

    @Test
    public void retornaValorPorChave(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");        
        assertEquals("555555", agenda.consultar("Bernardo"));
        assertEquals("444444",agenda.consultar("Mithrandir"));
    }

    @Test
    public void retornaVazio(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");        
        assertEquals(null, agenda.consultar("jonas"));
    }
    
    @Test
    public void retornoString(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Bernardo", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.adicionar("Jonas", "222222");  
        assertEquals("Bernardo,555555"+System.lineSeparator()+"Mithrandir,444444"+System.lineSeparator()+"Jonas,222222"+System.lineSeparator(), agenda.csv());
    }
    
    
    @Test
    public void buscaStringMasAAgendaEstaVazia(){
        AgendaContatos agenda = new AgendaContatos();        
        assertEquals(null, agenda.csv());
    }
}
