public class Funcionario{
    private String nome;
    private double salarioFixo;
    private double totalDeVendas;
    
    public Funcionario(String nome, double salarioFixo, double totalDeVendas){
        this.nome = nome;
        this.salarioFixo = salarioFixo;
        this.totalDeVendas = totalDeVendas;
    }
    
    public double getLucro(){
        double comissao = this.totalDeVendas - (this.totalDeVendas * 85/100.0);
        double salarioTotal = (this.salarioFixo - (this.salarioFixo * 10/100.0)) + comissao - (comissao * 10/100.0);
        return salarioTotal;
    }
}