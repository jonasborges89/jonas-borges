public class Numero {

    private int numero;

    public Numero(int numero){
        this.numero = numero;
    }

    public boolean impar(){
        if(this.numero % 2 > 0){
            return true;
        }
        return false;
    }

    public boolean verificarSomaDivisivel(Integer numero){
        if(numero == 0) return true;

        int soma = 0;

        while(numero > 0){
            soma += numero % 10;
            numero = numero / 10;
        }

        if( soma % this.numero == 0){
            return true;
        } 

        return false;
    }

}