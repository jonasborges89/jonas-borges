public class Numeros{

    private double[] numeros;

    public Numeros(double[] entrada){
        this.numeros = entrada;
    }

    public double[] calcularMediaSeguinte(){

        double[] retorno = new double[this.numeros.length - 1];
        for (int i =0; i < retorno.length; i++){
            retorno[i] = (this.numeros[i] + this.numeros[i + 1])/2;
        }
        
        return retorno;
    }
}