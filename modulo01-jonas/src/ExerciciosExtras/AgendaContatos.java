import java.util.HashMap;
import java.util.Map;
import java.util.LinkedHashMap;

public class AgendaContatos
{
    private LinkedHashMap<String, String> agenda = new LinkedHashMap<>();

    public void adicionar(String nome, String numero){
        this.agenda.put(nome,numero);
    }    

    public String consultar(String nome){
        return this.agenda.get(nome);
    }

    public String consultarPorNumero(String telefone){
        if(this.agenda.containsValue(telefone)){
            for(String key : this.agenda.keySet()){
                if(this.agenda.get(key).equals(telefone)){
                    return key; 
                }
            }
        } 
        return null;
    }

    public String csv(){

        boolean agendaVazia = this.agenda.isEmpty();
        
        if(!agendaVazia){
            
            StringBuilder retorno = new StringBuilder();
            String separador = System.lineSeparator();
            for (HashMap.Entry<String, String> entrada : this.agenda.entrySet()) {
                retorno.append( entrada.getKey() + "," + entrada.getValue() + separador);
            }

            return retorno.toString();
        }
        return null;
    }
}
